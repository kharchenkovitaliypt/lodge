package com.easymotion.artlife.model.storage

import android.net.Uri
import com.easymotion.artlife.BuildConfig
import io.realm.Realm
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.stubbing.OngoingStubbing
import org.mockito.stubbing.Stubber
import org.powermock.api.mockito.PowerMockito
import org.powermock.core.classloader.annotations.PowerMockIgnore
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.rule.PowerMockRule
import org.robolectric.RobolectricGradleTestRunner
import org.robolectric.annotation.Config
import rx.observers.TestSubscriber
import rx.schedulers.Schedulers
import javax.inject.Provider

//import org.assertj.core.api.Assertions.*

@RunWith(RobolectricGradleTestRunner::class)
@Config(constants = BuildConfig::class, sdk = intArrayOf(21))
@PowerMockIgnore("org.mockito.*")
@PrepareForTest(Realm::class)
class PersistentStorageTest {

    @Rule val rule = PowerMockRule();

    lateinit var mRealmMock: Realm
    lateinit var mStorage: PersistentStorage
    val KEY_DOG = "doggy"

    @Before
    fun setupUp() {
        mRealmMock = PowerMockito.mock(Realm::class.java);
        PowerMockito.mockStatic(Realm::class.java);
        wheN(Realm.getDefaultInstance()).thenReturn(mRealmMock);

        val realmProvider = Provider<Realm> { mRealmMock }
        val scheduler = Schedulers.immediate()
        mStorage = PersistentStorage(realmProvider, scheduler)
    }

    @Test fun testGet() {
        val uri1 = Uri.parse("http://www.dog.com/1")
        val uri2 = Uri.parse("http://www.dog.com/2")
        val uri3 = Uri.parse("http://www.dog.com/3")
        val uriList = listOf<Uri>(uri1, uri2, uri3)

        val subscriber = TestSubscriber<List<Uri>>()
        mStorage.get(KEY_DOG).subscribe(subscriber)
        subscriber.assertNoErrors()
        subscriber.assertNoValues()
    }

    @Test fun testAddAll() {

    }

    @Test fun testRemoveAll() {

    }

    /** Workaround for embellish code that substitutes [`when`] on [wheN] */
    fun <T> Stubber.wheN(methodCall: T): T = `when`(methodCall)
    fun <T> wheN(methodCall: T): OngoingStubbing<T> = `when`(methodCall)
}