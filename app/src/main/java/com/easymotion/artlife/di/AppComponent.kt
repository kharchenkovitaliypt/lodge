package com.easymotion.artlife.di

import com.easymotion.artlife.App
import com.easymotion.artlife.di.module.ActivityModule
import com.easymotion.artlife.di.module.RealmModule
import com.easymotion.artlife.di.scopes.AppScope
import com.easymotion.artlife.system.FrameRender
import dagger.Component

@AppScope
@Component(modules = arrayOf(RealmModule::class))
interface AppComponent {
    fun getActivityComponent(m: ActivityModule): ActivityComponent
    fun inject(inst: App)
    fun getFrameRender(): FrameRender
}