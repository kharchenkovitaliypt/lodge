package com.easymotion.artlife.di

import com.easymotion.artlife.di.module.ActivityModule
import com.easymotion.artlife.di.module.PresenterModule
import com.easymotion.artlife.di.scopes.ActivityScope
import com.easymotion.artlife.ui.screen.main.AbsWallpaperBaseActivity
import com.easymotion.artlife.ui.screen.main.page.inner.InnerPageFragment
import com.easymotion.artlife.wallpaper.screen.main.page.inner.LocalPageFragment
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = arrayOf(
        ActivityModule::class, PresenterModule::class
))
interface ActivityComponent {
    fun inject(inst: InnerPageFragment)
    fun inject(inst: LocalPageFragment)
    fun inject(inst: AbsWallpaperBaseActivity)
}