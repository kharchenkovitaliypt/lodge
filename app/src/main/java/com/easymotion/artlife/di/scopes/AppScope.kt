package com.easymotion.artlife.di.scopes

import javax.inject.Scope

@MustBeDocumented
@Scope
annotation class  AppScope