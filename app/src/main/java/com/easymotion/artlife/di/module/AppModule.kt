package com.easymotion.artlife.di.module

import android.content.Context
import android.content.SharedPreferences
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.preference.PreferenceManager
import android.util.Log
import com.easymotion.artlife.di.scopes.AppScope
import com.easymotion.artlife.interfaces.IResources
import com.easymotion.artlife.model.storage.IStorage
import com.easymotion.artlife.model.storage.PersistentStorage
import com.easymotion.artlife.net.DownloadManager
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import java.io.File

@Module
class AppModule(ctx: Context) {
    private val DL_FILES_DIR = "dl_files"

    private val mContext = ctx.applicationContext

    @Provides fun provideContext(): Context = mContext

    @Provides fun provideAssetManager(): AssetManager = mContext.assets

    @AppScope
    @Provides fun provideResources(): IResources {
        val res = mContext.resources
        return object : IResources {
            override fun getString(resId: Int): String = res.getString(resId)
            override fun getString(resId: Int, vararg args: Any): String = res.getString(resId, *args)
            override fun getText(resId: Int): CharSequence = res.getText(resId)
            override fun getInteger(resId: Int): Int = res.getInteger(resId)
            override fun getBitmap(resId: Int): Bitmap = BitmapFactory.decodeResource(res, resId)
        }
    }

    @AppScope
    @Provides fun provideEventBus(): EventBus = EventBus()

    @AppScope
    @Provides fun providePicasso(): Picasso {
        return Picasso.Builder(mContext)
                .loggingEnabled(true)
                .listener { picasso, uri, exception ->
                    Log.d("TAG", "Uri: $uri, exception: $exception")
                    exception.printStackTrace()
                }
                .build()
    }

    @AppScope
    @Provides fun provideSharedPreferences(): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(mContext)
    }

//    @Singleton
//    @Provides fun provideSettings(prefs: SharedPreferences, resCache: ResCache): Settings {
//        return Settings(prefs, resCache)
//    }

//    @Singleton
//    @Provides fun provideTweenAnimation(settings: Settings): TweenAnimation {
//        return TweenAnimation(settings)
//    }

    @AppScope
    @Provides fun provideDownloadManager(): DownloadManager {
        val dlFilesDir = File(mContext.filesDir, DL_FILES_DIR);
        val dm = DownloadManager(dlFilesDir);
        return dm
    }

    @AppScope @Provides
    fun provideUriCollection(inst: PersistentStorage): IStorage = inst
}
