package com.easymotion.artlife.di.module

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.support.v7.view.ActionMode
import com.easymotion.artlife.di.scopes.ActivityScope
import com.easymotion.artlife.interfaces.IActionModeHandler
import com.easymotion.artlife.interfaces.dialog.IDialogFactory
import com.easymotion.artlife.interfaces.dialog.impl.DialogFactoryImpl
import dagger.Module
import dagger.Provides
import java.lang.ref.WeakReference

@Module
class ActivityModule(val mActivity: AppCompatActivity) {

    @Provides fun provideActivity(): Activity = mActivity

    @ActivityScope
    @Provides fun provideActionModeHandler(): IActionModeHandler = object : IActionModeHandler {
        var mActionModeRef: WeakReference<ActionMode?>? = null

        override fun startActionMode(callback: ActionMode.Callback): ActionMode {
            val actionMode = mActivity.startSupportActionMode(callback)
            mActionModeRef = WeakReference(actionMode)
            return actionMode!!
        }
        override fun stopActionMode() {
            mActionModeRef?.get()?.finish()
            mActionModeRef = null
        }
        override fun getCurrentActionMode(): ActionMode? = mActionModeRef?.get()
    }

    @ActivityScope
    @Provides fun provideDialogFactory(impl: DialogFactoryImpl): IDialogFactory = impl

//    @Provides
//    fun provideLocalImageCollectionRequest(m: LocalImageCollection.Request): IImageCollection.IRequest = m
}