package com.easymotion.artlife.di.module

import android.content.Context
import android.os.Handler
import android.os.HandlerThread
import android.os.Process
import com.easymotion.artlife.di.scopes.AppScope
import dagger.Module
import dagger.Provides
import io.realm.Realm
import io.realm.RealmConfiguration
import rx.Scheduler
import rx.android.schedulers.HandlerScheduler

@Module(includes = arrayOf(AppModule::class))
class RealmModule {

    @AppScope @Provides fun provideRealm(ctx: Context): Realm {
        return Realm.getInstance(RealmConfiguration.Builder(ctx).build());
    }

    // Rx scheduler which will be performed all Realm's operations
    @AppScope @Provides fun provideRealmScheduler(): Scheduler {
        val realmThread = HandlerThread("RealmThread", Process.THREAD_PRIORITY_BACKGROUND)
        realmThread.start()
        val handler = Handler(realmThread.looper)
        return HandlerScheduler.from(handler)
    }
}