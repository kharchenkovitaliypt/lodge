package com.easymotion.artlife.di.module

import com.easymotion.artlife.ui.screen.main.IWallpaperBasePresenter
import com.easymotion.artlife.ui.screen.main.WallpaperBasePresenterImpl
import com.easymotion.artlife.ui.screen.main.page.inner.IInnerPagePresenter
import com.easymotion.artlife.ui.screen.main.page.inner.InnerPagePresenterImpl
import com.easymotion.artlife.wallpaper.screen.main.page.inner.ILocalPagePresenter
import com.easymotion.artlife.wallpaper.screen.main.page.inner.LocalPagePresenterImpl
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {
    @Provides fun provideWallpaperBasePresenter(p: WallpaperBasePresenterImpl): IWallpaperBasePresenter = p

    @Provides fun provideInnerPagePresenter(p: InnerPagePresenterImpl): IInnerPagePresenter = p
    @Provides fun provideLocalPagePresenter(p: LocalPagePresenterImpl): ILocalPagePresenter = p
}