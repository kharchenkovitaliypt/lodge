package com.easymotion.artlife.di.scopes

import javax.inject.Scope

@MustBeDocumented
@Scope
annotation class  ActivityScope