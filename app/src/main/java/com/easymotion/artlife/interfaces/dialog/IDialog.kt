package com.easymotion.artlife.interfaces.dialog

interface IDialog {
    fun show()
    fun dismiss()
}
