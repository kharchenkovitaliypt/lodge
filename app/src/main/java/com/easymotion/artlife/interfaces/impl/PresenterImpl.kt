package com.easymotion.artlife.interfaces.impl

/*
 * Copyright 2013 Square Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.os.Bundle
import com.easymotion.artlife.interfaces.IPresenter

// TODO Redact documentation
abstract class PresenterImpl<V> : IPresenter<V> {
    /**
     * Returns the view managed by this presenter, or null if [.takeView] has never been
     * called, or after [.dropView].
     */
    protected var view: V? = null
        private set

    /**
     * Called to give this presenter control of a view, typically from
     * [android.view.View.onAttachedToWindow]. Sets the
     * view that will be returned from [.getView].

     * It is expected that [.dropView] will be called with the same argument when the
     * view is no longer active, e.g. from [android.view.View.onDetachedFromWindow].
     */
    override fun takeView(view: V) {
        view ?: throw NullPointerException("new view must not be null")

        if (this.view !== view) {
            if (this.view != null) dropView(this.view!!)

            this.view = view
            onViewTaken(view)
        }
    }

    /** Called after the view became accessible */
    open fun onViewTaken(view: V) { }

    /**
     * Called to surrender control of this view, e.g. when the view is detached. If and only if
     * the given view matches the last passed to [.takeView], the reference to the view is
     * cleared.
     *
     *
     * Mismatched views are a no-op, not an error. This is to provide protection in the
     * not uncommon case that dropView and takeView are called out of order. For example, an
     * activity's views are typically inflated in [ ][android.app.Activity.onCreate], but are only detached some time after [ ][android.app.Activity.onDestroy]. It's possible for a view from one activity
     * to be detached well after the window for the next activity has its views inflatedthat
     * is, after the next activity's onResume call.
     */
    override fun dropView(view: V) {
        if (view == null) throw NullPointerException("dropped view must not be null")
        if (view === this.view) {
            onViewDropped(view)
            this.view = null
        }
    }

    /** Called right before the view would be inaccessible */
    open fun onViewDropped(view: V) { }

    /**
     * @return true if this presenter is currently managing a view, or false if [.takeView] has
     * * never been called, or after [.dropView].
     */
    protected fun hasNotView(): Boolean {
        return view == null
    }

    /**
     * Called only when [.getView] is not
     * null, and debounced. That is, this method will be called exactly once for a given view
     * instance, at least until that view is [dropped][.dropView].

     * See [.takeView] for details.
     */
    override fun onLoad(savedInstanceState: Bundle) {
    }

    override fun onSave(outState: Bundle) {
    }
}
