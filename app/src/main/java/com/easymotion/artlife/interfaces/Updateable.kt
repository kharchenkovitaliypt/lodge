package com.easymotion.artlife.interfaces

interface Updateable {
    fun update()
}
