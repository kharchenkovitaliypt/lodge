package com.easymotion.artlife.interfaces.dialog.impl

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.ViewGroup
import com.easymotion.artlife.R
import com.easymotion.artlife.interfaces.IResources
import com.easymotion.artlife.interfaces.dialog.IAlertDialog
import com.easymotion.artlife.interfaces.dialog.IDialog
import com.easymotion.artlife.interfaces.dialog.IDialogFactory
import java.util.*
import javax.inject.Inject

class DialogFactoryImpl
@Inject constructor(val mActivity: Activity,
                    val mResources: IResources)
: IDialogFactory {

    override fun showProgressDialog(messageRes: Int): IAlertDialog {
        return showProgressDialog(mResources.getText(messageRes))
    }

    override fun showProgressDialog(message: CharSequence): IAlertDialog {
        val dialog = ProgressDialogImpl(getActivity(), this)
        dialog.setCancelable(false)
        dialog.setMessage(message)
        dialog.show()
        return dialog
    }

    override fun createAlertDialog(): IAlertDialog {
        val dialog = AlertDialogImpl(getActivity(), this)
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(android.R.string.ok)) { dialog, which -> dialog.cancel() }
        dialog.show()
        return dialog
    }

    override fun showConfirmDialog(messageRes: Int, listener: () -> Unit): IAlertDialog {
        return showConfirmDialog(-1, messageRes, listener)
    }

    override fun showConfirmDialog(messageRes: Int,
                                     positiveButtonTitleRes: Int, negativeButtonTitleRes: Int,
                                     listener: () -> Unit): IAlertDialog {
        val dialog = AlertDialogImpl(getActivity(), this)
        dialog.setMessage(messageRes)
        dialog.setButton(Dialog.BUTTON_POSITIVE, getString(positiveButtonTitleRes)) { dialog, id -> listener() }
        dialog.setButton(Dialog.BUTTON_NEGATIVE, getString(negativeButtonTitleRes)) { dialog, id -> dialog.cancel() }
        dialog.show()
        return dialog
    }

    override fun showConfirmDialog(message: String, listener: () -> Unit): IAlertDialog {
        return showConfirmDialog(-1, message, listener)
    }

    override fun showConfirmDialog(titleRes: Int, messageRes: Int, listener: () -> Unit): IAlertDialog {
        return showConfirmDialog(titleRes, getString(messageRes), listener)
    }

    override fun showConfirmDialog(titleRes: Int, message: String?, listener: () -> Unit): IAlertDialog {
        val dialog = AlertDialogImpl(getActivity(), this)
        if (titleRes > 0) dialog.setTitle(titleRes)
        dialog.setMessage(message)
        dialog.setButton(Dialog.BUTTON_POSITIVE, getString(android.R.string.ok)) { dialog, id -> listener() }
        dialog.setButton(Dialog.BUTTON_NEGATIVE, getString(android.R.string.cancel)) { dialog, id -> dialog.cancel() }
        dialog.show()
        return dialog
    }

    override fun showErrorDialog(messageRes: Int): IAlertDialog {
        return showErrorDialog(getText(messageRes))
    }

    override fun showErrorDialog(message: CharSequence?): IAlertDialog {
        // TODO Add error icon
        //        dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
        //        dialog.setContentView(R.layout.custom_dialog);
        //        dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.your_icon);
        val dialog = createDialog(R.string.error, message)
        dialog.show()
        return dialog
    }

    override fun createDialog(titleRes: Int, messageRes: Int): IAlertDialog {
        return createDialog(titleRes, getText(messageRes))
    }

    override fun createDialog(titleRes: Int, message: CharSequence?): IAlertDialog {
        val dialog = createAlertDialog()
        dialog.setTitle(getText(titleRes))
        dialog.setMessage(message)
        return dialog
    }

    override fun createEditTextDialog(titleRes: Int, text: CharSequence, listener: IDialogFactory.OnEditTextCompleteListener): IDialog {
        throw UnsupportedOperationException()
//        val dialog = AlertDialogImpl(getActivity())
//
//        dialog.setTitle(titleRes)
//
//        val view = inflate(R.layout.dialog_edit_text, null)
//        dialog.setView(view)
//
//        val editText = view.findViewById(R.id.edit_text) as TextView
//        editText.text = text
//
//        dialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok)) { dialog, id ->
//            Keyboard.hideKeyboard(editText)
//
//            listener.onEditComplete(editText.text.toString())
//        }
//        dialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel)) { dialog, id ->
//            Keyboard.hideKeyboard(editText)
//
//            dialog.cancel()
//        }
//
//        return dialog
    }

    override fun createFromToDatesPicker(fromTitleRes: Int, toTitleRes: Int, listener: IDialogFactory.OnFromToDatesPickListener): IDialog {
        return createFromToDatesPicker(fromTitleRes, toTitleRes, -1, -1, listener)
    }

    override fun createFromToDatesPicker(fromTitleRes: Int, toTitleRes: Int, minDate: Long, maxDate: Long, listener: IDialogFactory.OnFromToDatesPickListener): IDialog {
        throw UnsupportedOperationException()
//        val dialog = AlertDialogImpl(getActivity())
//
//        val view = inflate(R.layout.dialog_from_to_dates_picker, null)
//        dialog.setView(view)
//
//        (view.findViewById(R.id.title_from) as TextView).setText(fromTitleRes)
//        val datePickerFrom = view.findViewById(R.id.date_picker_from) as DatePicker
//        if (minDate > 0) {
//            datePickerFrom.minDate = minDate
//        }
//        if (maxDate > 0) {
//            datePickerFrom.maxDate = maxDate
//        }
//
//        (view.findViewById(R.id.title_to) as TextView).setText(toTitleRes)
//        val datePickerTo = view.findViewById(R.id.date_picker_to) as DatePicker
//        if (minDate > 0) {
//            datePickerTo.minDate = minDate
//        }
//        if (maxDate > 0) {
//            datePickerTo.maxDate = maxDate
//        }
//
//        dialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok)) { dialog, id ->
//            val calendarFrom = Calendar.getInstance()
//            calendarFrom.set(Calendar.DAY_OF_MONTH, datePickerFrom.dayOfMonth)
//            calendarFrom.set(Calendar.MONTH, datePickerFrom.month)
//            calendarFrom.set(Calendar.YEAR, datePickerFrom.year)
//
//            val calendarTo = Calendar.getInstance()
//            calendarTo.set(Calendar.DAY_OF_MONTH, datePickerTo.dayOfMonth)
//            calendarTo.set(Calendar.MONTH, datePickerTo.month)
//            calendarTo.set(Calendar.YEAR, datePickerTo.year)
//
//            listener.onFromToDatesPicked(calendarFrom.time, calendarTo.time)
//        }
//        dialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel)) { dialog, id -> dialog.cancel() }
//
//        return dialog
    }

    override fun createDateTimePicker(titleRes: Int, dateTimeType: IDialogFactory.DateTimeType, initDate: Date, listener: IDialogFactory.OnDateTimePickListener): IDialog {
        return createDateTimePicker(getString(titleRes), dateTimeType, initDate, listener)
    }

    override fun createDateTimePicker(title: String?, dateTimeType: IDialogFactory.DateTimeType, initDate: Date, listener: IDialogFactory.OnDateTimePickListener): IDialog {
        throw UnsupportedOperationException()
//        val dialog = AlertDialogImpl(getActivity())
//
//        dialog.setTitle(title)
//
//        val view = inflate(R.layout.dialog_date_time_picker, null)
//        dialog.setView(view)
//
//        val datePicker = view.findViewById(R.id.date_picker) as DatePicker
//        val timePicker = view.findViewById(R.id.time_picker) as TimePicker
//        timePicker.setIs24HourView(true)
//
//        if (initDate != null) {
//            val c = Calendar.getInstance()
//            c.time = initDate
//
//            datePicker.init(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH), null)
//
//            timePicker.currentHour = c.get(Calendar.HOUR_OF_DAY)
//            timePicker.currentMinute = c.get(Calendar.MINUTE)
//        }
//
//        if (IDialogFactory.DateTimeType.DATE === dateTimeType) {
//            timePicker.visibility = View.GONE
//        } else if (IDialogFactory.DateTimeType.TIME === dateTimeType) {
//            datePicker.visibility = View.GONE
//        }
//
//        dialog.setButton(Dialog.BUTTON_POSITIVE, getString(R.string.ok)) { dialog, id ->
//            val calendar = Calendar.getInstance()
//            // Date
//            calendar.set(Calendar.DAY_OF_MONTH, datePicker.dayOfMonth)
//            calendar.set(Calendar.MONTH, datePicker.month)
//            calendar.set(Calendar.YEAR, datePicker.year)
//            //Time
//            calendar.set(Calendar.HOUR_OF_DAY, timePicker.currentHour)
//            calendar.set(Calendar.MINUTE, timePicker.currentMinute)
//
//            listener.onDateTimePicked(dateTimeType, calendar.time)
//        }
//        dialog.setButton(Dialog.BUTTON_NEGATIVE, getString(R.string.cancel)) { dialog, id -> dialog.cancel() }
//
//        return dialog
    }

//    override fun createListItemPickerDialog(titleRes: Int, itemsArray: Array<String>,
//                                            onClickListener: DialogInterface.OnClickListener): IAlertDialog {
//        val dialog = ListItemPickerDialogImpl(getActivity())
//        dialog.setTitle(titleRes)
//        dialog.setItems(itemsArray, onClickListener)
//        return dialog
//    }

    private class AlertDialogImpl(context: Context,
                                  val mResources: DialogFactoryImpl)
    : AlertDialog(context), IAlertDialog {

        override fun setTitle(titleRes: Int) {
            setTitle(mResources.getString(titleRes))
        }
        override fun setMessage(messageRes: Int) {
            setMessage(mResources.getString(messageRes))
        }
    }

    private class ProgressDialogImpl(context: Context,
                                     val mResources: DialogFactoryImpl)
    : ProgressDialog(context), IAlertDialog {

        override fun setMessage(messageRes: Int) {
            super.setMessage(mResources.getString(messageRes))
        }
    }

    private class ListItemPickerDialogImpl(context: Context) : IAlertDialog {
        val mBuilder = AlertDialog.Builder(context)
        var mDialog: Dialog? = null

        override fun setTitle(titleRes: Int) { mBuilder.setTitle(titleRes) }
        override fun setTitle(title: CharSequence?) { mBuilder.setTitle(title) }

        override fun setMessage(messageRes: Int) { mBuilder.setMessage(messageRes) }
        override fun setMessage(message: CharSequence?) { mBuilder.setMessage(message) }

        fun setItems(items: Array<String>, onClickListener: DialogInterface.OnClickListener) {
            mBuilder.setItems(items, onClickListener)
        }

        override fun show() {
            if(mDialog != null) throw IllegalStateException("The previous dialog isn't dismissed")
            mDialog = mBuilder.create()
            mDialog!!.show()
        }
        override fun dismiss() {
            mDialog?.dismiss()
            mDialog = null
        }
    }

    fun getActivity(): Activity = mActivity

    private fun getText(@StringRes resId: Int): CharSequence? {
        return if(resId > 0) mResources.getText(resId) else null
    }

    private fun getString(@StringRes resId: Int): String? {
        return if(resId > 0) mResources.getString(resId) else null
    }

    private fun inflate(layoutRes: Int, parent: ViewGroup?): View {
        return getActivity().layoutInflater.inflate(layoutRes, parent, false)
    }
}