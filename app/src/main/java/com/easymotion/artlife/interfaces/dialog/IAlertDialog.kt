package com.easymotion.artlife.interfaces.dialog

interface IAlertDialog : IDialog {

    fun setTitle(titleRes: Int)
    fun setTitle(title: CharSequence?)

    fun setMessage(messageRes: Int)
    fun setMessage(message: CharSequence?)
}
