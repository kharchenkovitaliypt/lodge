package com.easymotion.artlife.interfaces.dialog

import android.support.annotation.StringRes
import java.util.*

interface IDialogFactory {

    fun createDialog(@StringRes titleRes: Int, messageRes: Int): IAlertDialog
    fun createDialog(@StringRes titleRes: Int, message: CharSequence?): IAlertDialog

    fun createAlertDialog(): IAlertDialog

    fun showErrorDialog(messageRes: Int): IAlertDialog
    fun showErrorDialog(message: CharSequence?): IAlertDialog

    fun showProgressDialog(@StringRes messageRes: Int): IAlertDialog
    fun showProgressDialog(message: CharSequence): IAlertDialog

    fun showConfirmDialog(@StringRes titleRes: Int, @StringRes messageRes: Int, listener: () -> Unit): IAlertDialog
    fun showConfirmDialog(@StringRes titleRes: Int, message: String?, listener: () -> Unit): IAlertDialog
    fun showConfirmDialog(@StringRes messageRes: Int, listener: () -> Unit): IAlertDialog
    fun showConfirmDialog(@StringRes messageRes: Int,
                            @StringRes positiveButtonTitleRes: Int,
                            @StringRes negativeButtonTitleRes: Int, listener: () -> Unit): IAlertDialog
    fun showConfirmDialog(message: String, listener: () -> Unit): IAlertDialog
//    interface OnConfirmListener {
//        fun onConfirm()
//    }

//    fun createListItemPickerDialog(@StringRes titleRes: Int, itemsArray: Array<String>, listener: DialogInterface.OnClickListener): IAlertDialog

    fun createEditTextDialog(@StringRes titleRes: Int, value: CharSequence, l: OnEditTextCompleteListener): IDialog
    interface OnEditTextCompleteListener {
        fun onEditComplete(editedText: String)
    }

//    fun createMultiEditDialog(@StringRes titleRes: Int, fieldTitlesRes: IntArray, l: MultiEditCompleteListener): IDialog
//    interface MultiEditCompleteListener {
//        fun onMultiEditComplete(edits: Array<EditText>, dialog: IDialog)
//    }

    fun createFromToDatesPicker(@StringRes fromTitleRes: Int,
                                @StringRes toTitleRes: Int, listener: OnFromToDatesPickListener): IDialog
    fun createFromToDatesPicker(@StringRes fromTitleRes: Int,
                                @StringRes toTitleRes: Int, minDate: Long, maxDate: Long, listener: OnFromToDatesPickListener): IDialog
    interface OnFromToDatesPickListener {
        fun onFromToDatesPicked(from: Date, to: Date)
    }

    fun createDateTimePicker(@StringRes titleRes: Int, dateTimeType: DateTimeType, initDate: Date, listener: OnDateTimePickListener): IDialog
    fun createDateTimePicker(@StringRes title: String?, dateTimeType: DateTimeType, initDate: Date, listener: OnDateTimePickListener): IDialog
    enum class DateTimeType {
        TIME, DATE, DATE_TIME
    }
    interface OnDateTimePickListener {
        fun onDateTimePicked(dateTimeType: DateTimeType, dateTime: Date)
    }
}
