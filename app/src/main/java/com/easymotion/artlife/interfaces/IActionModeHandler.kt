package com.easymotion.artlife.interfaces

import android.support.v7.view.ActionMode

/** Interface for interaction with the action bar action mode */
interface IActionModeHandler {
    fun startActionMode(callback: ActionMode.Callback): ActionMode
    fun stopActionMode()
    fun getCurrentActionMode(): ActionMode?
}