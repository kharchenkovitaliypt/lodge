package com.easymotion.artlife.interfaces

import android.os.Bundle

interface IPresenter<V> {
    fun takeView(view: V)
    fun dropView(view: V)
    fun onLoad(savedInstanceState: Bundle)
    fun onSave(outState: Bundle)
}
