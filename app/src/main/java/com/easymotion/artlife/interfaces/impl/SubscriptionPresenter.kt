package com.easymotion.artlife.interfaces.impl

import rx.Subscription
import java.util.*


open class SubscriptionPresenter<V> : PresenterImpl<V>() {

    private val mSubsList = ArrayList<Subscription>(2)
    protected val subscriptions = Subscriptions()

    override fun onViewDropped(view: V) {
        mSubsList.forEach { it.unsubscribe() }
        mSubsList.clear()
    }

    /** Syntactic sugar for registering [Subscription] */
    inner class Subscriptions {

        operator fun plusAssign(s: Subscription) {
            if(hasNotView())
                s.unsubscribe() // No need anymore observe changes
            else
                mSubsList += s
        }
    }
}