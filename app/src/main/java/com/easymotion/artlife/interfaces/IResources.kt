package com.easymotion.artlife.interfaces

import android.graphics.Bitmap
import android.support.annotation.IntegerRes
import android.support.annotation.StringRes

/**
 * Interface for accessing the app resources
 */
interface IResources {
    fun getString(@StringRes resId: Int): String
    fun getString(@StringRes resId: Int, vararg args: Any): String
    fun getText(@StringRes resId: Int): CharSequence
    fun getInteger(@IntegerRes resId: Int): Int
    fun getBitmap(resId: Int): Bitmap
}