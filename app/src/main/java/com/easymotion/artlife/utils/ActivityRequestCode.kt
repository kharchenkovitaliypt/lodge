package com.easymotion.artlife.utils

/**
 * Support cross app unique activity request codes
 */
object ActivityRequestCode {
    @Volatile private var mCount = 0

    fun generate(): Int {
        return mCount++
    }
}
