package com.easymotion.artlife.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ObjectBox {
    private static final Map<String, Object> sString2Obj = new ConcurrentHashMap<>();

    public static String put(Object obj){
        String key = generateKey(obj);
        sString2Obj.put(key, obj);
        return key;
    }

    public static <T> T get(String key){
        return (T) sString2Obj.get(key);
    }

    public static <T> T remove(String key){
        return (T) sString2Obj.remove(key);
    }

    private static String generateKey(Object obj){
        return obj.toString();
    }
}
