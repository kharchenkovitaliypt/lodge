package com.easymotion.artlife.utils;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

public class IOUtils {

    public static String toStringQuiet(InputStream is){
        try {
            return toString(is);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String toString(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuilder total = new StringBuilder();
        String line;
        try {
            while ((line = br.readLine()) != null) {
                total.append(line);
            }
        }finally {
            br.close();
        }
        return total.toString();
    }

    public static byte[] getContent(InputStream is, boolean close) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream(is.available());
        return swap(baos, is, 4 * 1024, null, close) ? baos.toByteArray() : null;
    }

    public static boolean swap(OutputStream os, InputStream is, AsyncTask task) throws IOException {
        return swap(os, is, 4 * 1024, task, true);
    }

    public static boolean swap(OutputStream os, InputStream is, int bufferSize, AsyncTask task, boolean close) throws IOException {
        OutputStream bos = new BufferedOutputStream(os, bufferSize);
        try{
            byte[] buffer = new byte[8 * 1096];
            int n;
            while((n = is.read(buffer)) != -1){
                if(task != null && task.isCancelled())
                    return false;

                os.write(buffer, 0, n);
            }
            os.flush();
            return true;
        }
        finally {
            if(close) try { is.close(); } catch (IOException e) { e.printStackTrace(); }
            try { bos.close(); } catch (IOException e) { e.printStackTrace(); }
        }
    }

    public static boolean isOnline(Context ctx) {
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    private static final String SCHEME_ASSET = "file:///android_asset/";

    public static InputStream openStream(Context ctx, Uri uri) throws IOException {
        final String scheme = uri.getScheme();
        final String uriString = uri.toString();

        if(uriString.startsWith(SCHEME_ASSET)){
            String path = uriString.substring(SCHEME_ASSET.length());
            return ctx.getAssets().open(path);
        }
        else if (ContentResolver.SCHEME_CONTENT.equals(scheme)
                || ContentResolver.SCHEME_FILE.equals(scheme)) {

            return ctx.getContentResolver().openInputStream(uri);
        }
        throw new IllegalArgumentException("Scheme: " + scheme + " unsupported");
    }
}
