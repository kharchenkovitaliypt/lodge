package com.easymotion.artlife.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.util.Log
import com.easymotion.artlife.BuildConfig
import com.easymotion.artlife.R
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject

class ConfigResolver
@Inject
constructor() {
    private val TAG = ConfigResolver::class.java.simpleName
    private val DEBUG = true && BuildConfig.DEBUG
    private val DEBUG_SPENT_TIME = false && DEBUG

    private val SIZE_MAX_IMAGE_METADATA = 128 * 1024
    private val mOptions by lazy {
        val options = BitmapFactory.Options()
        options.inTempStorage = ByteArray(SIZE_MAX_IMAGE_METADATA)
        options
    }
    val MAX_WIDTH by lazy { mConfig.getInt(R.string.texture_max_width) }
    val MAX_HEIGHT by lazy { mConfig.getInt(R.string.texture_max_height) }

    @Inject lateinit var mContext: Context
    @Inject lateinit var mConfig: Config

    /**
     * Decodes [Bitmap] from the parameter URI(even supports android asset scheme 'file:///android_asset/')
     * with some modifications:
     * 1. Reduce [Bitmap] size(w x h) by function [ConfigResolver.calculateInSampleSize] with parameters
     * [ConfigResolver.MAX_WIDTH] x [ConfigResolver.MAX_HEIGHT]
     * 2. Rotate to upright if landscape
     *
     * @throws [IOException] when some problem with receive data from the parameter URI
     * */
    @Throws(IOException::class)
    fun resolveBitmap(uri: Uri): Bitmap {
        var time = System.currentTimeMillis()

        val inputStream = IOUtils.openStream(mContext, uri)
        inputStream.buffered(SIZE_MAX_IMAGE_METADATA)
        inputStream.mark(SIZE_MAX_IMAGE_METADATA)

        return inputStream.use {bis ->
            // Obtain only image size(without full decoding)
            mOptions.inJustDecodeBounds = true
            BitmapFactory.decodeStream(bis, null, mOptions)

            if (DEBUG_SPENT_TIME) {
                Log.d(TAG, "---------------------------------")
                Log.d(TAG, "resolveBitmap() Retrieve image size: " + (System.currentTimeMillis() - time) + "ms")
                time = System.currentTimeMillis()
            }
            if (DEBUG) Log.d(TAG, "resolveBitmap() input: " + mOptions.outWidth + 'x' + mOptions.outHeight)

            mOptions.inJustDecodeBounds = false // Decode full image
            // Calc zoom in factor
            mOptions.inSampleSize = calculateInSampleSize(mOptions, MAX_WIDTH, MAX_HEIGHT)

            val successReset = try {
                bis.reset()
                true
            } catch (e: IOException) {
                // TODO Explore bug
                Log.e(TAG, "resolveBitmap() Bug - unable to reset input stream")
                false
            }

            if(successReset) {
                return decodeAndFixRotation(uri, bis, mOptions)
            } else {
                val innerBis = IOUtils.openStream(mContext, uri)
                return innerBis.use {bis ->
                    decodeAndFixRotation(uri, bis, mOptions)
                }
            }
        }
    }

    private fun decodeAndFixRotation(uri: Uri, inputStream: InputStream, options: BitmapFactory.Options): Bitmap {
        val bitmap = BitmapFactory.decodeStream(inputStream, null, options)
        bitmap ?: throw IOException("Couldn't decode: " + uri)
        //if (DEBUG_SPENT_TIME) Log.d(TAG, "resolveBitmap() Decode with resize: " + (System.currentTimeMillis() - time) + "ms")
        val angle = getImageRotation(uri)
        //if (DEBUG) Log.d(TAG, "resolveBitmap() output: " + bitmap.width + 'x' + bitmap.height + ", rotation: " + angle + " degree")
        return if (angle > 0) rotateBitmap(bitmap, angle) else bitmap
    }

    private fun getImageRotation(uri: Uri): Float {
        try {
            val path = uri.schemeSpecificPart.substring("//".length)
            val exifInterface = ExifInterface(path)
            val orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, -1)
            val angle: Float
            when (orientation) {
                ExifInterface.ORIENTATION_UNDEFINED, ExifInterface.ORIENTATION_NORMAL -> angle = 0f
                ExifInterface.ORIENTATION_ROTATE_90 -> angle = 90f
                ExifInterface.ORIENTATION_ROTATE_180 -> angle = 180f
                ExifInterface.ORIENTATION_ROTATE_270 -> angle = 270f
                else -> {
                    Utils.resolveException(TAG, "Unsupported orientation: " + orientation)
                    angle = 0f
                }
            }
            return angle
        } catch (e: IOException) {
            Utils.resolveException(e)
            return 0f
        }

    }

    private val mMatrix = Matrix()

    private fun rotateBitmap(srcBitmap: Bitmap, angle: Float): Bitmap {
        val time = System.currentTimeMillis()
        mMatrix.reset()
        mMatrix.postRotate(angle)
        val rotated = Bitmap.createBitmap(srcBitmap,
                0, 0, srcBitmap.width, srcBitmap.height, mMatrix, true)

        if (DEBUG_SPENT_TIME)
            Log.d(TAG, "rotateBitmap() bitmap: " + srcBitmap.width + 'x' + srcBitmap.height
                    + "Spent time: " + (System.currentTimeMillis() - time) + "ms")
        srcBitmap.recycle()
        return rotated
    }

    private fun calculateInSampleSize(
            options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            return if (heightRatio < widthRatio) heightRatio else widthRatio
        } else {
            return 1
        }
    }
}
