package com.easymotion.artlife.utils;

import android.content.SharedPreferences;

import com.easymotion.artlife.R;
import com.easymotion.artlife.interfaces.IResources;

import javax.inject.Inject;

public class Settings {
    @Inject SharedPreferences mSharedPrefs;
    @Inject IResources mResources;

    @Inject
    public Settings() { }

    private SharedPreferences getPrefs(){
        return mSharedPrefs;
    }

    public int getImageChangeInterval(){
        return getInt(R.string.key_image_change_interval, R.integer.image_change_interval);
    }

    public int getImageChangeDuration(){
        return getInt(R.string.key_image_change_duration, R.integer.image_change_duration);
    }

    public int getBackgroundColor(){
        return getInt(R.string.key_background_color, R.integer.background_color);
    }

    private int getInt(int keyRes, int defValueRes){
        return getPrefs().getInt(mResources.getString(keyRes),
                mResources.getInteger(defValueRes));
    }

//    public int getCameraMovementInterval(){
//        return getPrefs().getInt(mResCache.getString(R.string.key_camera_movement_interval),
//                mResCache.getInt(R.integer.camera_movement_interval));
//    }
//
//    public int getCameraMovementDuration(){
//        return getPrefs().getInt(mResCache.getString(R.string.key_camera_movement_duration),
//                mResCache.getInt(R.integer.camera_movement_duration));
//    }

    public float getCameraDistance(){
        return 1.01f;
    }

    public int getMaxFrameNumber(){
        return 1;
    }
}
