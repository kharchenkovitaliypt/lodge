package com.easymotion.artlife.utils

import android.util.SparseArray

import com.easymotion.artlife.interfaces.IResources

import javax.inject.Inject

class Config
@Inject
constructor() {

    private val mObjectCache = SparseArray<Any>()
    @Inject lateinit var mResContainer: IResources

    @Synchronized private fun getString(res: Int): String {
        var value: String? = mObjectCache.get(res) as String?
        if (value == null) {
            value = mResContainer.getString(res)
            mObjectCache.put(res, value)
        }
        return value
    }

    fun getInt(res: Int): Int {
        return Integer.parseInt(getString(res))
    }

    fun getLong(res: Int): Long = getString(res).toLong()

    fun getFloat(res: Int): Float {
        return java.lang.Float.parseFloat(getString(res))
    }

    fun getDouble(res: Int): Double {
        return java.lang.Double.parseDouble(getString(res))
    }

    fun getBoolean(res: Int): Boolean {
        return java.lang.Boolean.parseBoolean(getString(res))
    }

    fun getStringList(res: Int): List<String> {
        return getString(res)
                .split(',')
                .map { it.trim() }
                .filterNot { it.isEmpty() }
    }
}
