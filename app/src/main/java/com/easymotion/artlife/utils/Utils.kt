package com.easymotion.artlife.utils

import android.util.Log
import com.easymotion.artlife.BuildConfig
import java.util.*

object Utils {
    private val RANDOM = Random()
    //    public static final int SECOND = 1000;
    /**
     * @param min
     * @param max must be more than min
     * @return random value in range from min to max
     */
    fun randomInRange(min: Double, max: Double): Double {
        if (max < min) {
            throw IllegalArgumentException("Max is less than min: $min > $max")
        }
        val range = max - min
        val scaled = Math.random() * range
        return scaled + min
    }

    fun nextInt(max: Int): Int {
        return RANDOM.nextInt(max)
    }

    fun resolveException(e: Exception) {
        if (BuildConfig.DEBUG) {
            throw RuntimeException(e)
        } else
            e.printStackTrace()
    }

    fun resolveException(tag: String, e: String) {
        if (BuildConfig.DEBUG) {
            throw RuntimeException(e)
        } else
            Log.w(tag, e)
    }

    fun <T> removeAll(collection: Collection<T>, removesItems: Collection<T>): Collection<T> {
        val items = ArrayList(collection)
        items.removeAll(removesItems)
        return items
    }
}

fun <T> Iterator<T>.toList(): List<T> {
    val list = ArrayList<T>()
    for(item in this) list.add(item)
    return list
}
