//package com.easymotion.artlife.wallpaper.net;
//
//import android.support.annotation.NonNull;
//
//import java.util.List;
//
//import retrofit.RestAdapter;
//import retrofit.http.GET;
//import retrofit.http.Query;
//
//public class InstagramNetSource implements INetSource{
//    private static final String ACCESS_TOKEN = "1512588038.1fb234f.3ca713faccc944f598ecbfcd6a10c6bf";
//
//    private final InstagramService mInstagramService;
//    private final String mAccessToken;
//
//    private InstagramNetSource(InstagramService instagramService, String accessToken){
//        mInstagramService = instagramService;
//        mAccessToken = accessToken;
//    }
//
//    public static InstagramNetSource create(@NonNull String config){
//        RestAdapter restAdapter = new RestAdapter.Builder()
//                .setEndpoint("https://api.instagram.com/v1")
//                .build();
//
//        InstagramService instagramService = restAdapter.create(InstagramService.class);
//
//        return new InstagramNetSource(instagramService, ACCESS_TOKEN);
//    }
//
//    @Override
//    public String getName() {
//        return "Instagram";
//    }
//
//    @Override
//    public List<String> getImageURLsFrom(long time) {
//        return mInstagramService.listPopular(ACCESS_TOKEN);
//    }
//
//    interface InstagramService {
//
//        @GET("/media/popular")
//        List<String> listPopular(@Query("access_token") String access_token);
//    }
//}
