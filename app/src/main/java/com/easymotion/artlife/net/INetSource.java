package com.easymotion.artlife.net;

import java.util.List;

public interface INetSource {

    String getName();

    List<String> getImageURLsFrom(long fromTime);
}
