package com.easymotion.artlife.net

import android.net.Uri
import android.os.SystemClock
import android.util.Log
import com.easymotion.artlife.BuildConfig
import java.io.File
import java.io.IOException
import java.net.URL
import java.util.*
import java.util.concurrent.CopyOnWriteArrayList
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class DownloadManager(private val mFilesDir: File) {
    val TAG = DownloadManager::class.java.simpleName

    private var mLifeCount: Int = 0 // running if more then zero

    private val mNetSourceContainers = CopyOnWriteArrayList<NetSourceContainer>()

    private val mRequestInterval = 5 * 60 * 1000.toLong()
    @Volatile private var mLastRequestTime: Long = -1

    private val mService = Executors.newSingleThreadScheduledExecutor()

    private val mDownloadImagesTask = Runnable {
        if (BuildConfig.DEBUG) Log.d(TAG, "DownloadTask.run()")

        for (nsc in mNetSourceContainers) {
            for (url in nsc.newImageURLs) {
                val di = download(url)
                if (di != null) nsc.put(di)
            }
        }
        onDownloadFinish()
    }

    private fun download(url: String): DownloadManager.DatedImage? {
        try {
            val name = url.hashCode().toString()
            val file = File(mFilesDir, name)
            file.createNewFile()

            val inputStream = URL(url).openStream()
            inputStream.copyTo(file.outputStream())

            return DownloadManager.DatedImage(Uri.fromFile(file), file.lastModified())

        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }

    }

    val imageURIs: Collection<Uri>
        get() {
            val uris = ArrayList<Uri>()
            for (nsc in mNetSourceContainers) uris.addAll(nsc.imageURIs)

            return uris
        }

    fun setNetSources(netSources: Collection<INetSource>) {
        mNetSourceContainers.clear()
        for (ns in netSources) {
            mNetSourceContainers.add(DownloadManager.NetSourceContainer(ns))
        }
    }

    fun start() {
        ++mLifeCount
        updateState()
    }

    fun stop() {
        --mLifeCount
        updateState()
    }

    val isRunning: Boolean
        get() = mLifeCount > 0

    private fun updateState() {
        val idleTime = SystemClock.elapsedRealtime() - mLastRequestTime

        if (BuildConfig.DEBUG) Log.d(TAG, "updateState() isRunning: " + isRunning + ", idle time: " + idleTime / 1000 + "sec")

        if (isRunning) {
            if (mLastRequestTime == -1L || idleTime >= mRequestInterval) {
                mService.execute(mDownloadImagesTask)
            }
        } else {
            //            mService.shutdownNow();
        }
    }

    private fun onDownloadFinish() {
        mLastRequestTime = SystemClock.elapsedRealtime()

        mService.schedule(mDownloadImagesTask, mRequestInterval, TimeUnit.MILLISECONDS)
    }

    internal class NetSourceContainer(val netSource: INetSource) {
        private val mDatedImages = HashSet<DatedImage>()

        val imageURIs: Collection<Uri>
            get() {
                val uris = ArrayList<Uri>()
                for (di in mDatedImages) uris.add(di.imageURI)

                return uris
            }

        val newImageURLs: Collection<String>
            get() = netSource.getImageURLsFrom(lastImageTime)

        val lastImageTime: Long
            get() {
                var lastImageTime = java.lang.Long.MAX_VALUE
                for (di in mDatedImages) {
                    if (di.time < lastImageTime) lastImageTime = di.time
                }
                return lastImageTime
            }

        fun put(di: DownloadManager.DatedImage) {
            mDatedImages.add(di)
        }
    }

    internal class DatedImage(val imageURI: Uri, val time: Long) {
        override fun equals(other: Any?): Boolean{
            if (this === other) return true
            if (other?.javaClass != javaClass) return false
            other as DownloadManager.DatedImage

            return imageURI != other.imageURI
        }

        override fun hashCode(): Int{
            return imageURI.hashCode()
        }
    }
}
