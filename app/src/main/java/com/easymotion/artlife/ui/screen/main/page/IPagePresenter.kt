package com.easymotion.artlife.wallpaper.screen.main.mvp

import android.net.Uri
import com.easymotion.artlife.interfaces.IPresenter

interface IPagePresenter : IPresenter<IPageView> {
    fun onImageSelect(uri: Uri)
    fun removeImageList(items: List<Uri>)
}
