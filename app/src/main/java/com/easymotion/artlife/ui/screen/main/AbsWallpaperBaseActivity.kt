package com.easymotion.artlife.ui.screen.main

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import butterknife.Bind
import butterknife.ButterKnife
import butterknife.OnPageChange
import com.easymotion.artlife.App
import com.easymotion.artlife.R
import com.easymotion.artlife.di.ActivityComponent
import com.easymotion.artlife.di.module.ActivityModule
import com.easymotion.artlife.interfaces.IActionModeHandler
import com.easymotion.artlife.interfaces.IResources
import com.easymotion.artlife.system.ActivityResultHandler
import com.easymotion.artlife.ui.screen.main.page.inner.InnerPageFragment
import com.easymotion.artlife.wallpaper.screen.main.page.inner.LocalPageFragment
import com.viewpagerindicator.TitlePageIndicator
import javax.inject.Inject

open class AbsWallpaperBaseActivity : AppCompatActivity(), IWallpaperBaseView { // OnActivityResultObservable {
    private val TAG = AbsWallpaperBaseActivity::class.java.simpleName

    private lateinit var mComponent: ActivityComponent
    @Inject lateinit var mPresenter: IWallpaperBasePresenter
    @Inject lateinit var mRes: IResources
    @Inject lateinit var mActivityResultHandler: ActivityResultHandler
    @Inject lateinit var mActionModeHandler: IActionModeHandler

    @Bind(R.id.viewPager)          lateinit var mViewPager: ViewPager
    @Bind(R.id.titlePageIndicator) lateinit var mTitlePageIndicator: TitlePageIndicator

    fun getComponent(): ActivityComponent = mComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        // Init dependency injection components before any initializations
        mComponent = (application as App).getAppComponent()
                .getActivityComponent(ActivityModule(this));
        mComponent.inject(this)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        // Take the view(MVP) only once because presenter lifetime is the same as the Activity
        mPresenter.takeView(this)
    }

    fun initView() {
        ButterKnife.bind(this) // Inject views
        mViewPager.adapter = SlidePagerAdapter(supportFragmentManager, mRes)
        mTitlePageIndicator.setViewPager(mViewPager)
    }

    /**
     * Options menu
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when(item.itemId) {
            R.id.action_advanced_settings -> {
                mPresenter.onAdvancedSettings()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mActivityResultHandler.onResult(requestCode, resultCode, data)

        super.onActivityResult(requestCode, resultCode, data)
    }

    @OnPageChange(R.id.viewPager) fun onPageChange() {
        mActionModeHandler.stopActionMode() // Dismiss the action mode when fragments are switched
    }

    /** Page adapter */
    class SlidePagerAdapter(fm: FragmentManager,
                            val mRes: IResources) : FragmentStatePagerAdapter(fm) {
        val mFragments: Array<Pair<Int, Fragment>> = arrayOf(
                R.string.storage_inner to InnerPageFragment(),
                R.string.storage_local to LocalPageFragment()
        )
        override fun getPageTitle(pos: Int): String = mRes.getString(mFragments[pos].first)
        override fun getItem(pos: Int): Fragment = mFragments[pos].second
        override fun getCount(): Int = mFragments.size
    }
}