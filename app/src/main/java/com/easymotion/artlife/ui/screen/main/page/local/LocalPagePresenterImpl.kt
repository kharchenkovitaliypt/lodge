package com.easymotion.artlife.wallpaper.screen.main.page.inner

import android.net.Uri
import com.easymotion.artlife.model.imagecollection.LocalImageCollection
import com.easymotion.artlife.wallpaper.screen.main.mvp.AbsPagePresenter
import javax.inject.Inject


class LocalPagePresenterImpl @Inject constructor() : ILocalPagePresenter, AbsPagePresenter() {

    @Inject lateinit var mCollectionRequest: LocalImageCollection.Request
    @Inject lateinit override var imageCollection: LocalImageCollection

    override fun onAddImages() {
        mCollectionRequest.addImageList()
    }

    override fun removeImageList(items: List<Uri>) {
        imageCollection.removeImageList(items)
    }
}