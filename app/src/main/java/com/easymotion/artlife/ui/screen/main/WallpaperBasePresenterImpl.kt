package com.easymotion.artlife.ui.screen.main

import android.app.Activity
import android.content.Intent
import com.easymotion.artlife.interfaces.impl.PresenterImpl
import com.easymotion.artlife.ui.screen.main.additional.SettingsActivity
import javax.inject.Inject

class WallpaperBasePresenterImpl @Inject constructor()
: IWallpaperBasePresenter, PresenterImpl<IWallpaperBaseView>() {

    @Inject lateinit var mActivity: Activity

    override fun onAdvancedSettings() {
        mActivity.startActivity(Intent(mActivity, SettingsActivity::class.java)) // TODO move to Navigation
    }
}
