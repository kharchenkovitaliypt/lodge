package com.easymotion.artlife.ui.screen.main.page.inner

import android.content.Context
import android.net.Uri
import android.widget.Toast
import com.easymotion.artlife.model.imagecollection.InnerImageCollection
import com.easymotion.artlife.wallpaper.screen.main.mvp.AbsPagePresenter
import javax.inject.Inject


class InnerPagePresenterImpl @Inject constructor() : IInnerPagePresenter, AbsPagePresenter() {

    @Inject lateinit override var imageCollection: InnerImageCollection
    @Inject lateinit var mContext: Context

    override fun removeImageList(items: List<Uri>) {
        // throw UnsupportedOperationException()
        Toast.makeText(mContext, "Removing...", Toast.LENGTH_LONG).show() // TODO Only for presentation purposes
    }
}