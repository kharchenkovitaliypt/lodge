//package com.easymotion.artlife.wallpaper.dialog;
//
//import android.annotation.SuppressLint;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.util.SparseBooleanArray;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.AbsListView;
//import android.widget.ArrayAdapter;
//import android.widget.CheckBox;
//import android.widget.CompoundButton;
//import android.widget.ImageView;
//import android.widget.TextView;
//
//import com.easymotion.artlife.R;
//import com.extensionlib.dialog.RetainedDialogFragment;
//import com.squareup.picasso.Picasso;
//
//import org.greenrobot.eventbus.EventBus;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.List;
//
//import butterknife.Bind;
//import butterknife.ButterKnife;
//
//public class ImageGroupFragment extends RetainedDialogFragment implements DialogInterface.OnClickListener {
//    private static final String ARG_ITEMS = "items";
//
//    private final SparseBooleanArray mCheckedItems = new SparseBooleanArray();
//
//    private AbsListView mAdapterView;
//    private ImageAdapter mImageAdapter;
//    private List<AURL> mItems;
//    private EventBus mBus;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//
//    }
//
//    public static ImageGroupFragment newInstance(Collection<AURL> items){
//        ImageGroupFragment f = new ImageGroupFragment();
//        Bundle args = new Bundle();
//        args.putSerializable(ARG_ITEMS, new ArrayList<>(items));
//        f.setArguments(args);
//        return f;
//    }
//
//    @SuppressLint("InflateParams")
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        View view = getActivity().getLayoutInflater()
//                            .inflate(R.layout.listview, null, false);
//        mAdapterView =(AbsListView) view;
//
//        mItems =(List<AURL>) getArguments().getSerializable(ARG_ITEMS);
//
//        mImageAdapter = new ImageAdapter(getActivity(), mItems, mCheckedItems){
//            @Override
//            public boolean isEnabled(int position) {
//                return false;
//            }
//        };
//        mAdapterView.setAdapter(mImageAdapter);
//        mAdapterView.setEmptyView(view.findViewById(android.R.id.empty));
////        mAdapterView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
////            @Override
////            public void onItemClick(AdapterView<?> adapterView, View view, int pos, long l) {
////                boolean checked = mCheckedItems.get(pos);
////                mCheckedItems.put(pos, !checked);
////                ((ImageAdapter.ViewHolder) view.getTag()).checkBox.setChecked(!checked);
////            }
////        });
//
//        return new AlertDialog.Builder(getActivity())
//                .setView(view)
//                .setTitle(R.string.check_items)
//                .setPositiveButton(android.R.string.ok, this)
//                .setNegativeButton(android.R.string.cancel, this)
//                .create();
//    }
//
//    @Override
//    public void onDestroyView() {
//        super.onDestroyView();
//        mAdapterView = null;
//    }
//
//    @Override
//    public void onClick(DialogInterface dialogInterface, int buttonId) {
//        if(DialogInterface.BUTTON_POSITIVE == buttonId){
//
//            Collection<AURL> uris = new ArrayList<>();
//            for(int i=0, N=mItems.size(); i < N; i++){
//                boolean checked = mCheckedItems.get(i, false);
//                if(checked){
//                    uris.add(mItems.get(i));
//                }
//            }
//            mBus.post(new CheckedItemsEvent(uris));
//        }
//
//        dismissAllowingStateLoss();
//    }
//
//    /**
//     * Event bus checked items event
//     */
//    public class CheckedItemsEvent{
//        public final Collection<AURL> items;
//
//        CheckedItemsEvent(Collection<AURL> items){
//            this.items = items;
//        }
//    }
//
//    /**
//    ** View adapter
//     */
//    static class ImageAdapter extends ArrayAdapter<AURL> {
//        private final LayoutInflater mLayoutInflater;
//        private final SparseBooleanArray mCheckedItems;
//
//        ImageAdapter(Context context, Collection<AURL> items, SparseBooleanArray checkedItems) {
//            super(context, R.layout.item_image, items.toArray(new AURL[items.size()]));
//            mLayoutInflater = LayoutInflater.from(context);
//            mCheckedItems = checkedItems;
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            if(convertView == null){
//                convertView = mLayoutInflater.inflate(R.layout.item_default_image_group, parent, false);
//            }
//
//            ViewHolder viewHolder =(ViewHolder) convertView.getTag();
//            if(viewHolder == null){
//                viewHolder = new ViewHolder(convertView);
//                convertView.setTag(viewHolder);
//            }
//
//            AURL uri = getItem(position);
//
//            viewHolder.textView.setText(uri.getName());
//            viewHolder.checkBox.setTag(position); // set current checkbox position in the list
//
//            Picasso.with(getContext())
//                    .load(uri.toURI())
//                    .placeholder(R.drawable.ic_file)
//                    .into(viewHolder.imageView);
//
//            return convertView;
//        }
//
//        private final CompoundButton.OnCheckedChangeListener mCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
//                int position =(Integer) compoundButton.getTag();
//                mCheckedItems.put(position, checked);
//            }
//        };
//
//        public class ViewHolder{
//            @Bind(R.id.ivImage) ImageView imageView;
//            @Bind(R.id.tvTitle) TextView textView;
//            @Bind(android.R.id.checkbox) CheckBox checkBox;
//
//            ViewHolder(View view){
//                ButterKnife.bind(this, view);
//                checkBox.setOnCheckedChangeListener(mCheckedChangeListener);
//            }
//        }
//    }
//}
