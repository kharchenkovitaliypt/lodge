package com.easymotion.artlife.wallpaper.screen.main.mvp

import android.net.Uri

interface IPageView {

    fun showImageList(items: List<Uri>)
}