package com.easymotion.artlife.ui.screen.main.additional;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.preference.DialogPreference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.easymotion.artlife.BuildConfig;
import com.easymotion.artlife.R;

public class NumberPickerPreference extends DialogPreference {
    private static final int NO_VALUE = -1;

    private int mMinValue, mMaxValue, mDefaultValue;
    private int mUnitsRes;

    private NumberPicker mNumberPicker;

    public NumberPickerPreference(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray numberPickerType = context.obtainStyledAttributes(attrs,
                R.styleable.NumberPickerPreference, 0, 0);

        mMaxValue = numberPickerType.getInt(R.styleable.NumberPickerPreference_maxValue, NO_VALUE);
        mMinValue = numberPickerType.getInt(R.styleable.NumberPickerPreference_minValue, NO_VALUE);
        mDefaultValue = numberPickerType.getInt(R.styleable.NumberPickerPreference_defaultValue, NO_VALUE);
        mUnitsRes = numberPickerType.getResourceId(R.styleable.NumberPickerPreference_units, NO_VALUE);

        if (mMaxValue == NO_VALUE || mMinValue == NO_VALUE || mDefaultValue == NO_VALUE){
            throw new IllegalStateException("maxValue, minValue and defaultValue must to be setup");
        }
        if(mUnitsRes == NO_VALUE) {
            throw new IllegalStateException("units must to be setup");
        }
        numberPickerType.recycle();
    }

    @Override
    protected void onAttachedToHierarchy(PreferenceManager preferenceManager) {
        super.onAttachedToHierarchy(preferenceManager);
        setSummary(getPersistedInt(mDefaultValue));
    }

    @SuppressLint("InflateParams")
    @Override
    protected View onCreateDialogView() {
        if(BuildConfig.DEBUG) Log.d("TAG", "onCreateDialogView() defaultValue: " + getPersistedInt(-1));

        int max = mMaxValue;
        int min = mMinValue;

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.number_picker_dialog, null);

        mNumberPicker = (NumberPicker) view.findViewById(R.id.number_picker);
        // Initialize state
        mNumberPicker.setMaxValue(max);
        mNumberPicker.setMinValue(min);
        mNumberPicker.setWrapSelectorWheel(false);
        mNumberPicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mNumberPicker.setValue(getPersistedInt(mDefaultValue));

        ((TextView) view.findViewById(R.id.tvUnits)).setText(mUnitsRes);

        return view;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult) {
        if (positiveResult) {
            int value = mNumberPicker.getValue();
            persistInt(value);
            setSummary(value);
        }
    }

    public void setSummary(int value){
        setSummary(value + " " + getContext().getString(mUnitsRes));
    }
}
