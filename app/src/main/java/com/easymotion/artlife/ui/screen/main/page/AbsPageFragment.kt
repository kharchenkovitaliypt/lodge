package com.easymotion.artlife.ui.screen.main.page

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.view.ActionMode
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.*
import android.widget.ImageView
import butterknife.Bind
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnLongClick
import com.bignerdranch.android.multiselector.ModalMultiSelectorCallback
import com.bignerdranch.android.multiselector.MultiSelector
import com.bignerdranch.android.multiselector.SwappingHolder
import com.easymotion.artlife.R
import com.easymotion.artlife.interfaces.IActionModeHandler
import com.easymotion.artlife.interfaces.IResources
import com.easymotion.artlife.interfaces.dialog.IDialogFactory
import com.easymotion.artlife.wallpaper.screen.main.mvp.IPagePresenter
import com.easymotion.artlife.wallpaper.screen.main.mvp.IPageView
import com.squareup.picasso.Picasso
import java.util.*
import javax.inject.Inject

/** Showing the images of a specific group with possibility to manipulate by them  */
abstract class AbsPageFragment : Fragment(), IPageView {

    val mMultiSelector = MultiSelector();

    @Inject lateinit var mPicasso: Picasso
    @Inject lateinit var mActionModeHandler: IActionModeHandler
    @Inject lateinit var mDialogFactory: IDialogFactory
    @Inject lateinit var mResources: IResources

    @Bind(R.id.recycler_view)
    lateinit var mRecyclerView: RecyclerView;

    abstract fun getPresenter(): IPagePresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        getPresenter().takeView(this)
    }

    override fun onDestroyView() {
        getPresenter().dropView(this)
        ButterKnife.unbind(this) // Release injected views (set nulls)
        super.onDestroyView()
    }

    fun initView() {
        ButterKnife.bind(this, view) // Inject views
        // Init RecyclerView
        mRecyclerView.layoutManager = GridLayoutManager(activity, 3) // TODO Make an auto columns calculation depending on the screen size
        mRecyclerView.adapter = Adapter(getPresenter(),
                mPicasso, mMultiSelector, mActionModeHandler, mDialogFactory, mResources)
    }

    override fun showImageList(items: List<Uri>) {
        (mRecyclerView.adapter as Adapter).setItems(items)
    }

    /** Adapter */
    class Adapter(// Bug Dagger 2 complain about an inner class
            val mPresenter: IPagePresenter,
            val mPicasso: Picasso,
            val mMultiSelector: MultiSelector,
            val mActionModeHandler: IActionModeHandler,
            val mDialogFactory: IDialogFactory,
            val mResources: IResources)
    : RecyclerView.Adapter<Adapter.ViewHolder>() {

        private val mItems = ArrayList<Uri>()

        fun setItems(items: List<Uri>) {
            mItems.clear()
            mItems.addAll(items)
            notifyDataSetChanged()
        }

        override fun onCreateViewHolder(parentView: ViewGroup, pos: Int): Adapter.ViewHolder? {
            val view = LayoutInflater.from(parentView.context)
                    .inflate(R.layout.item_image, parentView, false)
            return Adapter.ViewHolder(view, mMultiSelector, mPresenter, mActionModeHandler, mActionMode)
        }

        override fun onBindViewHolder(vh: Adapter.ViewHolder, pos: Int) {
            val uri = mItems[pos]
            mPicasso.load(uri)
                    .placeholder(R.drawable.ic_file)
                    .fit()
                    .centerInside()
                    .into(vh.itemView as ImageView)
        }

        override fun getItemCount() = mItems.size

        /** ViewHolder */
        class ViewHolder(view: View,
                        // Bug: Dagger 2 complain about an inner class
                         val mMultiSelector: MultiSelector,
                         val mPresenter: IPagePresenter,
                         var mActionModeHandler: IActionModeHandler,
                         val mActionModeCallback: ActionMode.Callback)
        : SwappingHolder(view, mMultiSelector) {

            init { ButterKnife.bind(this, itemView) }

            @OnClick(R.id.image) fun onClick() {
                val isActionMode = mMultiSelector.tapSelection(this)
                val selectedPositions = mMultiSelector.selectedPositions // Selected items indices
                if (isActionMode) {
                    if(selectedPositions.isEmpty()) {
                        mActionModeHandler.stopActionMode() // Hide the action mode if there is no selected items
                    } else {
                        setActionModeTitle(mActionModeHandler.getCurrentActionMode(), selectedPositions.size)
                    }
                } else {
                    // Handle a click when the action mode is off
                    // mPresenter.onImageSelect() TODO
                }
            }
            @OnLongClick(R.id.image) fun onLongClick(): Boolean {
                var actionMode = mActionModeHandler.startActionMode(mActionModeCallback);
                setActionModeTitle(actionMode, 1) // First item was selected
                mMultiSelector.setSelected(this, true);
                return true
            }
            fun setActionModeTitle(actionMode: ActionMode?, itemsAmount: Int) {
                actionMode?.title = itemsAmount.toString()
            }
        }

        /** Showing and handling actions upon the items list */
        val mActionMode = object : ModalMultiSelectorCallback(mMultiSelector) {

            override fun onCreateActionMode(actionMode: ActionMode, menu: Menu?): Boolean {
                super.onCreateActionMode(actionMode, menu)
                actionMode.menuInflater.inflate(R.menu.images_list_operations, menu)
                return true
            }
            override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {
                return when (item.itemId) {
                    R.id.action_delete -> {
                        mode.finish()
                        // Filter out selected items
                        val selectedItems = mItems.filterIndexed { i, uri -> mMultiSelector.isSelected(i, 0) }
                        removeImageList(selectedItems)
                        true
                    }
                    else -> false
                }
            }
        }

        fun removeImageList(items: List<Uri>) {
            val question = mResources.getString(R.string.question_remove_images, items.size) //
            mDialogFactory.showConfirmDialog(question) { mPresenter.removeImageList(items) }
        }
    }
}
