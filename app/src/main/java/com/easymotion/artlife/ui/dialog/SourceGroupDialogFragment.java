//package com.easymotion.artlife.ui.dialog;
//
//import android.annotation.SuppressLint;
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.os.Bundle;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.widget.AdapterView;
//import android.widget.ListView;
//
//import com.easymotion.artlife.R;
//import com.easymotion.artlife.ui.adapter.SourceAdapter;
//import com.easymotion.artlife.wallpaper.service.IImageStorage;
//import com.extensionlib.dialog.RetainedDialogFragment;
//
//public class SourceGroupDialogFragment extends RetainedDialogFragment implements AdapterView.OnItemClickListener {
//
//    private SourceAdapter mSourceAdapter;
////    @Inject
//    IImageStorage mImageStorage;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //Injector.INSTANCE.getAppComponent().inject(this);
//    }
//
//    @SuppressLint("InflateParams")
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        ListView listView =(ListView) LayoutInflater.from(getActivity())
//                .inflate(R.layout.listview, null);
//
//        mSourceAdapter = new SourceAdapter(getActivity(), null); // FIXME mImageStorage.getSources());
//
//        listView.setAdapter(mSourceAdapter);
//        listView.setOnItemClickListener(this);
//
//        return new AlertDialog.Builder(getActivity())
//                .setTitle(R.string.image_source)
//                .setView(listView)
//                .create();
//    }
//
//    @Override
//    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
////        mSourceAdapter.getItem(i)
////                .showSettings(getActivity());
//
//        dismissAllowingStateLoss();
//    }
//}
