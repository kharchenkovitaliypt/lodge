package com.easymotion.artlife.ui.screen.main.page.inner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.easymotion.artlife.R
import com.easymotion.artlife.ui.screen.main.AbsWallpaperBaseActivity
import com.easymotion.artlife.ui.screen.main.page.AbsPageFragment
import com.easymotion.artlife.wallpaper.screen.main.mvp.ILocalPageView
import com.easymotion.artlife.wallpaper.screen.main.mvp.IPagePresenter
import javax.inject.Inject

class InnerPageFragment : ILocalPageView, AbsPageFragment() {

    @Inject lateinit var mPresenter: IInnerPagePresenter

    override fun getPresenter(): IPagePresenter = mPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AbsWallpaperBaseActivity).getComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_page, container, false)
    }
}