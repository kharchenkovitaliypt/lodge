package com.easymotion.artlife.ui.screen;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.WallpaperManager;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.easymotion.artlife.system.PhotoWallpaperService;

/** It helps the user to set the live wallpaper */
public class LWChooseHelperActivity extends Activity {

    @TargetApi(16)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Build.VERSION.SDK_INT >= 16) {
            // Directly showing the live wallpaper
            ComponentName component = new ComponentName(getPackageName(), PhotoWallpaperService.class.getCanonicalName());
            Intent intent = new Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER);
            intent.putExtra(WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT, component);
            startActivity(intent);
        } else {
            // Showing live wallpapers chooser
            startActivity(new Intent(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER));
        }
        finish();
    }
}