package com.easymotion.artlife.wallpaper.screen.main.page.inner

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.OnClick
import com.easymotion.artlife.R
import com.easymotion.artlife.ui.screen.main.AbsWallpaperBaseActivity
import com.easymotion.artlife.wallpaper.screen.main.mvp.ILocalPageView
import com.easymotion.artlife.wallpaper.screen.main.mvp.IPagePresenter
import com.easymotion.artlife.ui.screen.main.page.AbsPageFragment
import javax.inject.Inject

class LocalPageFragment : ILocalPageView, AbsPageFragment() {

    @Inject lateinit var mPresenter: ILocalPagePresenter

    override fun getPresenter(): IPagePresenter = mPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AbsWallpaperBaseActivity).getComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_local_page, container, false)
    }

    @OnClick(R.id.fab) fun onAddClick() {
        mPresenter.onAddImages()
    }
}