package com.easymotion.artlife.ui.screen.main

import com.easymotion.artlife.interfaces.IPresenter

interface IWallpaperBasePresenter : IPresenter<IWallpaperBaseView> {

    fun onAdvancedSettings()
}
