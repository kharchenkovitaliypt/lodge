package com.easymotion.artlife.ui.screen.main.additional;

import android.os.Bundle;
import android.preference.PreferenceActivity;

import com.easymotion.artlife.R;

public class SettingsActivity extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        /*
         ** Security issue http://securityintelligence.com/new-vulnerability-android-framework-fragment-injection/#.U_zcPXVJV5Q
          */
        String initialFragment = getIntent().getStringExtra(EXTRA_SHOW_FRAGMENT);
        if(initialFragment != null){
            throw new IllegalArgumentException("Invalid fragment for this activity: " + initialFragment);
        }

        super.onCreate(savedInstanceState);
        //noinspection deprecation
        addPreferencesFromResource(R.xml.main_preferences);

//        Intent intent = new Intent(this, FileChooserActivity.class);
//        intent.putExtra(FileChooserActivity.EXTRA_MIME_TYPES, new String[]{ MimeTypeMap.MIME_TYPE_IMAGE });
//        startActivity(intent);
    }
}
