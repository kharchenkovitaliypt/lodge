package com.easymotion.artlife.wallpaper.screen.main.mvp

import android.net.Uri
import com.easymotion.artlife.interfaces.impl.SubscriptionPresenter
import com.easymotion.artlife.model.imagecollection.IImageCollection
import rx.android.schedulers.AndroidSchedulers

/** Take care about displaying and updating image list on the view from the data model [IImageCollection] */
abstract class AbsPagePresenter
: IPagePresenter, SubscriptionPresenter<IPageView>() {

    abstract val imageCollection: IImageCollection

    override fun onViewTaken(view: IPageView) {
        super.onViewTaken(view)
        subscriptions += imageCollection.state
                .startWith(IImageCollection.State.NOTHING)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { update() }
        update()
    }

    fun update() {
        subscriptions += imageCollection.imageList
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {items -> view!!.showImageList(items) }
    }

    override fun onImageSelect(uri: Uri) {
//        ImageDialogFragment.newInstance(uri.toURI())
//                .show(((Activity) view.getContext()).getFragmentManager(), null);
    }
}
