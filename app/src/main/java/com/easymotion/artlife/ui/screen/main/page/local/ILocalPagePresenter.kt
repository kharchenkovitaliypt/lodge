package com.easymotion.artlife.wallpaper.screen.main.page.inner

import com.easymotion.artlife.wallpaper.screen.main.mvp.IPagePresenter


interface ILocalPagePresenter : IPagePresenter {

    fun onAddImages()
}