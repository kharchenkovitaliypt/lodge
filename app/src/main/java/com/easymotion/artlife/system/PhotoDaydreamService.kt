package com.easymotion.artlife.system

import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.badlogic.gdx.backends.android.AndroidDaydream
import com.easymotion.artlife.App

class PhotoDaydreamService : AndroidDaydream() {

    override fun onDreamingStarted() {
        val render = (application as App).getAppComponent().getFrameRender();

        initialize(render, getConfiguration())

        super.onDreamingStarted()
    }

    /**
     * Creates suitable configuration for the Daydream service
     * @return the [AndroidApplicationConfiguration]
     */
    internal fun getConfiguration(): AndroidApplicationConfiguration {
        val config = AndroidApplicationConfiguration()
        config.a = 0
        config.r = 0
        config.g = 0
        config.b = 0

        config.useCompass = false
        config.useAccelerometer = false
        return config
    }
}
