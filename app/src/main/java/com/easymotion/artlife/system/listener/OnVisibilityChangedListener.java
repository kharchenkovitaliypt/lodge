package com.easymotion.artlife.system.listener;

public interface OnVisibilityChangedListener {

    void onVisibilityChanged(boolean visible);
}
