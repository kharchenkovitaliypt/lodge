package com.easymotion.artlife.system

import android.util.Log
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService
import com.easymotion.artlife.App
import com.easymotion.artlife.BuildConfig
import com.easymotion.artlife.system.listener.OnOffsetsChangedListener
import com.easymotion.artlife.system.listener.OnVisibilityChangedListener

class PhotoWallpaperService : AndroidLiveWallpaperService() {
    lateinit var mRender: FrameRender

    override fun onCreate() {
        super.onCreate()
        mRender = (application as App).getAppComponent().getFrameRender();
    }

    override fun onCreateEngine(): Engine {
        if (BuildConfig.DEBUG) Log.d("PhotoWallpaperService", "onCreateEngine()")
        return initEngine(ExtendedEngine())
    }

    override fun onCreateApplication() {
        super.onCreateApplication()
        if (BuildConfig.DEBUG) Log.d("PhotoWallpaperService", "onCreateApplication()")

        val config = AndroidApplicationConfiguration()
        config.useCompass = false
        config.useAccelerometer = false

        initialize(mRender, config)
    }

    private fun initEngine(engine: PhotoWallpaperService.ExtendedEngine): PhotoWallpaperService.ExtendedEngine {
        engine.setOnOffsetsChangedListener(mRender.offsetsHandler)
        engine.setOnVisibilityChangedListener(mRender)
        return engine
    }

    inner class ExtendedEngine : AndroidWallpaperEngine() {
        private val DEBUG_CALLS = false

        @Volatile private var mOnOffsetsChangedListener: OnOffsetsChangedListener? = null
        @Volatile private var mOnVisibilityChangedListener: OnVisibilityChangedListener? = null

        fun setOnOffsetsChangedListener(l: OnOffsetsChangedListener) {
            if (BuildConfig.DEBUG) Log.d("ExtendedEngine", "setOnOffsetsChangedListener() listener: " + l)
            mOnOffsetsChangedListener = l
        }

        fun setOnVisibilityChangedListener(l: OnVisibilityChangedListener) {
            if (BuildConfig.DEBUG) Log.d("ExtendedEngine", "setOnVisibilityChangedListener() listener: " + l)
            mOnVisibilityChangedListener = l
        }

        override fun onOffsetsChanged(xOffset: Float, yOffset: Float, xOffsetStep: Float, yOffsetStep: Float, xPixelOffset: Int, yPixelOffset: Int) {
            super.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset)

            if (mOnOffsetsChangedListener != null) {
                mOnOffsetsChangedListener!!.onOffsetsChanged(xOffset, yOffset, xOffsetStep, yOffsetStep, xPixelOffset, yPixelOffset)
            }
        }

        override fun onVisibilityChanged(visible: Boolean) {
            if (mOnVisibilityChangedListener != null) {
                mOnVisibilityChangedListener!!.onVisibilityChanged(visible)
            }
            if (DEBUG_CALLS) Log.d("ExtendedEngine", "> onVisibilityChanged(visible: $visible)")
            super.onVisibilityChanged(visible)
            if (DEBUG_CALLS) Log.d("ExtendedEngine", "< onVisibilityChanged()")
        }

        override fun isVisible(): Boolean {
            if (DEBUG_CALLS) Log.d("ExtendedEngine", ">>> - isVisible()")
            val visible = super.isVisible()
            if (DEBUG_CALLS) Log.d("ExtendedEngine", "<<< - isVisible()")
            return visible
        }

        override fun onResume() {
            super.onResume()
            mRender.offsetsHandler.setPreviewMode(isPreview) // Fix preview zero offset
        }
    }
}
