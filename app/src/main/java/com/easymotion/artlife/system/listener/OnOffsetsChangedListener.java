package com.easymotion.artlife.system.listener;

public interface OnOffsetsChangedListener {

    void onOffsetsChanged(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset);
}
