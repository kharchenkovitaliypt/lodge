package com.easymotion.artlife.system

import android.net.Uri
import android.util.Log
import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.utils.CameraInputController
import com.easymotion.artlife.BuildConfig
import com.easymotion.artlife.model.FrameBox
import com.easymotion.artlife.model.IScheduler
import com.easymotion.artlife.model.animation.TweenAnimation
import com.easymotion.artlife.model.animation.TweenAnimation.AnimationStatus
import com.easymotion.artlife.model.image.EasyTextureBox
import com.easymotion.artlife.model.imagecollection.InnerImageCollection
import com.easymotion.artlife.model.imagecollection.LocalImageCollection
import com.easymotion.artlife.system.listener.OnVisibilityChangedListener
import com.easymotion.artlife.system.utils.INormalized
import com.easymotion.artlife.system.utils.Lock
import com.easymotion.artlife.system.utils.OffsetsHandler
import com.easymotion.artlife.utils.ConfigResolver
import com.easymotion.artlife.utils.Settings
import com.easymotion.artlife.utils.toList
import org.greenrobot.eventbus.Subscribe
import rx.Observable
import java.util.*
import javax.inject.Inject

class FrameRender
@Inject
constructor(private val mTweenAnimation: TweenAnimation,
            private val mSettings: Settings,
            private val mConfigResolver: ConfigResolver,
            private val mInnerImageCollection: InnerImageCollection,
            private val mLocalImageCollection: LocalImageCollection)
: ApplicationListener, IScheduler, INormalized, OnVisibilityChangedListener {

    private val TAG = FrameRender::class.java.simpleName
    private val DEBUG = false && BuildConfig.DEBUG

    //    private static final int ANIMATION_FRAME_RATE = 60;
    //    private static final int ANIMATION_FRAME_DELAY = 1000 / ANIMATION_FRAME_RATE;
    //    private final FPSLogger mLogger = new FPSLogger();

    @Volatile private var mVisible: Boolean = false

    private var mCameraController: CameraInputController? = null
    private val mCamera = OrthographicCamera()

    private lateinit var mModelBatch: ModelBatch

    @Volatile
    private lateinit var mFrameBox: FrameBox

    private val mLock = Lock()
    val offsetsHandler = OffsetsHandler(mLock, this)

    private var mScreenNormalizedWidth: Float = 0F

    private val mColor = Color()

    fun getAllImageUriList(): List<Uri> {
        return Observable.merge(mInnerImageCollection.imageList,
                                mLocalImageCollection.imageList)
                         .flatMap { Observable.from(it) }
                         .toBlocking()
                         .iterator.toList()
    }

    override fun create() {
        //        float aspectRatio = (float) Gdx.graphics.getWidth() / (float) Gdx.graphics.getHeight();
        //        mCamera = new PerspectiveCamera(67, 1f * aspectRatio, 1f);
        //        mCamera.position.set(0f, 0f, 1.00001f);
        //        mCamera.position.set(0f, 0f, 1f);
        //        mCamera.lookAt(0f, 0f, 0);
        //        mCamera.near = 1f;
        //        mCamera.far = 300f;

        resize(Gdx.graphics.width, Gdx.graphics.height)

        if (DEBUG) {
            mCameraController = CameraInputController(mCamera)
            Gdx.input.inputProcessor = mCameraController
        }

        mModelBatch = ModelBatch()

        //mImageStorage.registerObserver(this);
        mTweenAnimation.register(this)

        val imageBox = EasyTextureBox(getAllImageUriList(), mConfigResolver)
        mFrameBox = FrameBox(this, mSettings, imageBox, mTweenAnimation)

        offsetsHandler.setCamera(mCamera)
        offsetsHandler.setFrameNormalized(mFrameBox)

        loadSettings()
    }

    override fun render() {
        if (DEBUG) mCameraController!!.update()

        mTweenAnimation.update(Gdx.graphics.deltaTime)

        Gdx.gl.glClearColor(mColor.r, mColor.g, mColor.b, mColor.a)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)

        mFrameBox.update()

        mModelBatch.begin(mCamera)
        mFrameBox.render(mModelBatch)
        mModelBatch.end()

        checkSleep()
    }

    override fun getNormalizedWidth(): Float = mScreenNormalizedWidth

    private fun checkSleep() {
        try {
            while (mLock.isRequested && mVisible) {
                if (BuildConfig.DEBUG) Log.d(TAG, "wait()")
                mLock.check()
                if (BuildConfig.DEBUG) Log.d(TAG, "awake()")
            }
            //            Thread.sleep(ANIMATION_FRAME_DELAY); // Use full fps
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

    }

    /*    *//**//**//**//**//**//**//**/
    /**
     * Subscribers
     */
    /**//**//**//**//**//**//**//*
    private Runnable mSourceEventRunnable = new Runnable() {
        @Override
        public void run() {
            updateURIs();
        }
    };

    @Subscribe
    public void onSourceEvent(IImageCollection event) {
        if(BuildConfig.DEBUG) Log.d(TAG, "onSourceEvent( " + event + ')');
        Gdx.app.postRunnable(mSourceEventRunnable);
    }**/

    @Subscribe
    fun onAnimationStatusEvent(status: AnimationStatus) {
        //        if(BuildConfig.DEBUG) Log.d(TAG, "onAnimationStatusEvent( " + status + ')');
        if (AnimationStatus.START == status) {
            mLock.wake()
        } else if (AnimationStatus.COMPLETE == status) {
            mLock.request()
        }
    }

    private fun loadSettings() {
        // Color
        val argb = mSettings.backgroundColor
        val r = android.graphics.Color.red(argb) / 255f
        val g = android.graphics.Color.green(argb) / 255f
        val b = android.graphics.Color.blue(argb) / 255f
        var a = android.graphics.Color.alpha(argb) / 255f
        if (a == 0f) a = 1f // Prevent missed an alpha value
        mColor.set(r, g, b, a)
        //        if(BuildConfig.DEBUG) Log.d(TAG, "mColor: " + mColor + ", alpha: " + mColor.a);
    }

    override fun resize(width: Int, height: Int) {
        mScreenNormalizedWidth = width.toFloat() / height.toFloat()

        mCamera.viewportWidth = mScreenNormalizedWidth
        mCamera.viewportHeight = 1f
        mCamera.update()
    }

    override fun resume() {
        loadSettings()
        mFrameBox.setImageURIs(getAllImageUriList()) // TODO Add check was updated or not
    }

    override fun pause() {
        mTimer.cancel()
    }

    override fun dispose() {
        //mImageStorage.unregisterObserver(this);
        mTweenAnimation.unregister(this)

        mFrameBox.dispose()
        mModelBatch.dispose()

        mTimer.cancel()
        mTimer.purge()
    }

    override fun onVisibilityChanged(visible: Boolean) {
        mVisible = visible
        mLock.wake()
    }

    /**
     * Scheduler
     */
    private val mTimer = Timer()

    override fun requestUpdate(afterInterval: Long) {
        if (BuildConfig.DEBUG) Log.d(TAG, "requestUpdate() afterInterval: " + afterInterval + 's')
        mTimer.schedule(object : TimerTask() {
            override fun run() {
                mLock.wake()
            }
        }, afterInterval * 1000L)
    }
}


