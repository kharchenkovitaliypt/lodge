package com.easymotion.artlife.system

import android.app.Activity
import android.content.Intent
import com.easymotion.artlife.di.scopes.ActivityScope
import java.util.*
import javax.inject.Inject

/**
 * Allow any class handle an Activity call result
 * Receiver must be registered for receiving the call result
 * */
@ActivityScope
class ActivityResultHandler
@Inject constructor(private val mActivity: Activity) {

    private val mListeners = HashSet<OnActivityResultListener>()

    fun startActivity(intent: Intent, requestCode: Int) {
        mActivity.startActivityForResult(intent, requestCode)
    }

    fun register(l: ActivityResultHandler.OnActivityResultListener) {
        mListeners += l
    }

    fun onResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mListeners.forEach { it.onActivityResult(requestCode, resultCode, data) }
    }

    interface OnActivityResultListener {
        fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)
    }
}