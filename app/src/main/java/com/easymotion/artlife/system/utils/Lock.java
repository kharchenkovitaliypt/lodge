package com.easymotion.artlife.system.utils;

public class Lock {
    private volatile boolean mLocked = false;

    public void request(){
        mLocked = true;
    }

    public synchronized void check() throws InterruptedException {
        wait(); // Falls asleep
    }

    public boolean isRequested() {
        return mLocked;
    }

    public synchronized void wake() {
        mLocked = false;
        notify(); // Wake up
    }
};
