package com.easymotion.artlife.system.utils;

public interface INormalized {

    float getNormalizedWidth();
}
