package com.easymotion.artlife.system.utils;

import android.support.v4.util.Pools;
import android.util.Log;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.easymotion.artlife.BuildConfig;
import com.easymotion.artlife.system.listener.OnOffsetsChangedListener;

public class OffsetsHandler implements OnOffsetsChangedListener {
    private static final float OVER_SCROLL = 0.7f;
    private final Lock mLock;
    private final INormalized mScreen;
    private volatile Camera mCamera;
    private volatile INormalized mFrame;
    private volatile boolean mIsPreview;

    public OffsetsHandler(Lock l, INormalized screenNormalized){
        mLock = l;
        mScreen = screenNormalized;
    }

    public void setCamera(Camera camera){
        mCamera = camera;
    }

    public void setFrameNormalized(INormalized frameNormalized){
        mFrame = frameNormalized;
    }

    public void setPreviewMode(boolean preview){
        mIsPreview = preview;
    }

    /**
     *  Call happening in UI(main) thread
     */
    @Override
    public synchronized void onOffsetsChanged(final float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
        if(BuildConfig.DEBUG) Log.d("OffsetsHandler", "xOffset: " + xOffset + ", xPixelOffset: " + xPixelOffset + ", app: " + Gdx.app);
        if(Gdx.app == null) return;

        Gdx.app.postRunnable(mOffsetsChangedTaskPool
                .acquire()
                .setXOffset(mIsPreview ? 0.5f : xOffset));
        if(mLock.isRequested()) mLock.wake();
    }

    /**
     * Pool
     */
    private final Pools.Pool<OffsetsChangedTask> mOffsetsChangedTaskPool = new Pools.SynchronizedPool<OffsetsChangedTask>(16){
        @Override
        public OffsetsChangedTask acquire() {
            OffsetsChangedTask instance = super.acquire();
            if(instance == null){
                instance = new OffsetsChangedTask();
            }
            return instance;
        }
    };

    class OffsetsChangedTask implements Runnable{
        private float xOffset; // 0.0 ... 1.0

        OffsetsChangedTask setXOffset(float xOffset){
            this.xOffset = xOffset;
            return this;
        }

        @Override
        public void run() {
            final float screenWidth = mScreen.getNormalizedWidth();
            final float frameWidth = mFrame.getNormalizedWidth();

            float remain = frameWidth - screenWidth;
            if(remain > 0){
                final float screenOverScroll = screenWidth * OVER_SCROLL;
                float baseOffset = 0;
                if(remain > screenOverScroll){
                    baseOffset = (remain - screenOverScroll) / 2f;
                    remain = screenOverScroll; // restrict scrolling
                }
                mCamera.position.x =
                        (-frameWidth / 2f) + screenWidth / 2f
                        + baseOffset + remain * xOffset;
                mCamera.update();
            }
            mOffsetsChangedTaskPool.release(this);
        }
    }
}
