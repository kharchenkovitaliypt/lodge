package com.easymotion.artlife

import android.app.Application
import com.easymotion.artlife.di.AppComponent
import com.easymotion.artlife.di.DaggerAppComponent
import com.easymotion.artlife.di.module.AppModule
import timber.log.Timber

class App : Application() {

    private lateinit var mAppComponent: AppComponent

    fun getAppComponent() = mAppComponent

    override fun onCreate() {
        super.onCreate()

        mAppComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()
        mAppComponent.inject(this)

        if (BuildConfig.DEBUG)  Timber.plant(Timber.DebugTree());
    }
}


