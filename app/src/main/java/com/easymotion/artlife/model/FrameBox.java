package com.easymotion.artlife.model;

import android.net.Uri;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.utils.Disposable;
import com.easymotion.artlife.utils.Settings;
import com.easymotion.artlife.model.animation.TweenAnimation;
import com.easymotion.artlife.model.image.ITextureBox;
import com.easymotion.artlife.system.utils.INormalized;

import java.util.Collection;

public class FrameBox implements Disposable, INormalized {
    private final Settings mSettings;

    private final Frame mFrame;
    private final ITextureBox mTextureBox;
//    private final TweenAnimation mTweenAnimation;

    public FrameBox(IScheduler scheduler, Settings settings, ITextureBox imageBox, TweenAnimation tweenAnimation) {
        mTextureBox = imageBox;
//        mTweenAnimation = tweenAnimation;
        mSettings = settings;
        mFrame = new Frame(scheduler, settings, mTextureBox.obtainTexture());
        mFrame.setTweenAnimation(tweenAnimation);
    }

    @Override
    public float getNormalizedWidth(){
        return mFrame.getNormalizedWidth();
    }

    public void setImageURIs(Collection<Uri> items) {
        mTextureBox.setImageURIs(items);
    }

    public void render(ModelBatch modelBatch) {
        mFrame.render(modelBatch);
    }

    public void update() {
        boolean needChangeImage = mTextureBox.getTextureCount() > 1
                                      && !mFrame.isAnimated()
                                      && (mFrame.getTexture() == null ||
                                          mFrame.getImageLifetime() > mSettings.getImageChangeInterval());
        if(needChangeImage) {
            Texture texture = mTextureBox.obtainTexture();
            mFrame.setTexture(mTextureBox, texture, true);
        }
    }

    @Override
    public void dispose() {
        mFrame.dispose();
        mTextureBox.dispose();
    }
}
