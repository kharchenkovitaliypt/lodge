package com.easymotion.artlife.model;

public interface ISize {

    int getWidth();

    int getHeight();
}
