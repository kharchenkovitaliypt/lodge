package com.easymotion.artlife.model.imagecollection

import android.content.res.AssetManager
import android.net.Uri
import com.easymotion.artlife.R
import com.easymotion.artlife.di.scopes.AppScope
import com.easymotion.artlife.interfaces.IResources
import com.easymotion.artlife.model.storage.IStorage
import javax.inject.Inject

@AppScope
class InnerImageCollection
@Inject
constructor(uriStorage: IStorage, res: IResources,
            private val mAssetManager: AssetManager)
: AbsImageCollection(uriStorage, res.getString(R.string.storage_inner)) {

    private val ASSETS_SCHEME = "file:///android_asset"
    private val IMAGE_DIR = "image"

    override val mStorageKey: String = "inner"

    /**
     * Returns all images URIs from the android assets directory [InnerImageCollection.IMAGE_DIR]
     */
    override val defaultUriList: List<Uri> =
            mAssetManager.list(IMAGE_DIR).map {name ->
            Uri.parse("$ASSETS_SCHEME/$IMAGE_DIR/$name") }
}
