package com.easymotion.artlife.model.animation;

import android.util.Log;

import com.easymotion.artlife.BuildConfig;
import com.easymotion.artlife.model.Frame;

import java.util.Arrays;

public class FrameAccessor implements TweenAccessorExt<Frame>{
    private static final String TAG = FrameAccessor.class.getSimpleName();
    private static final boolean DEBUG = false && BuildConfig.DEBUG;

    private static final int ATTRIBUTES_NUMBER = 1;

    public static final int TYPE_TRANSPARANCY = 1;

    @Override
    public int getValues(Frame target, int tweenType, float[] returnValues) {
        if(DEBUG) Log.d(TAG, "getValues() tweenType: " + tweenType + ", returnValues: " + Arrays.toString(returnValues));
        switch (tweenType){
            case TYPE_TRANSPARANCY:
                returnValues[0] = target.getOpacity();
                return ATTRIBUTES_NUMBER;
            default:
                throw new UnsupportedOperationException("Unsupported tween type: " + tweenType);
        }
    }

    @Override
    public void setValues(Frame target, int tweenType, float[] newValues) {
        if(DEBUG) Log.d(TAG, "setValues() tweenType: " + tweenType + ", newValues: " + Arrays.toString(newValues));
        switch (tweenType){
            case TYPE_TRANSPARANCY:
                target.setOpacity(newValues[0]);
                break;
            default:
                throw new UnsupportedOperationException("Unsupported tween type: " + tweenType);
        }

    }

    public int getValuesNumber(){
        return ATTRIBUTES_NUMBER;
    }
}
