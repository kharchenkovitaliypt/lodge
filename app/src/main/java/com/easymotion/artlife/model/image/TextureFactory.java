package com.easymotion.artlife.model.image;

import android.net.Uri;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.easymotion.artlife.utils.ConfigResolver;

import org.jetbrains.annotations.Nullable;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.IntBuffer;

public class TextureFactory {
    private static IntBuffer sIntBuffer = ByteBuffer.allocateDirect(4)
            .order(ByteOrder.nativeOrder())
            .asIntBuffer();

    @Nullable
    public static Texture createTexture(Uri uri, ConfigResolver resolver) {
        Gdx.gl.glGetIntegerv(GL20.GL_TEXTURE_BINDING_2D, sIntBuffer);
        int currentTexture = sIntBuffer.get(0);
        try {
            BitmapTextureData textureData = new BitmapTextureData(uri, resolver);
            Texture texture = new Texture(textureData);
            if (!textureData.isInitialized()) return null;

            texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

            return texture;
        } finally {
            Gdx.gl.glBindTexture(GL20.GL_TEXTURE_2D, currentTexture);
        }
    }
}
