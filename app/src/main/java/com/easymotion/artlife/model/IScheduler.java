package com.easymotion.artlife.model;

public interface IScheduler {
    /**
     * @param afterInterval in seconds
     */
    void requestUpdate(long afterInterval);
}
