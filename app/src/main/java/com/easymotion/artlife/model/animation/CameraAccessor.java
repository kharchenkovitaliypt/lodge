package com.easymotion.artlife.model.animation;

import android.util.Log;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.math.Vector3;
import com.easymotion.artlife.BuildConfig;
import com.easymotion.artlife.model.Frame;

import java.util.Arrays;

public class CameraAccessor implements TweenAccessorExt<Camera>{
    private static final String TAG = CameraAccessor.class.getSimpleName();
    private static final boolean DEBUG = false && BuildConfig.DEBUG;

    public static final int POS_X = 0;
    public static final int POS_Y = POS_X + 1;
    public static final int POS_Z = POS_X + 2;

    public static final int DIRECTION_X = POS_Z + 1;
    public static final int DIRECTION_Y = DIRECTION_X + 1;
    public static final int DIRECTION_Z = DIRECTION_X + 2;

    public static final int VALUES_NUMBER = DIRECTION_Z + 1;

    @Override
    public int getValues(Camera target, int tweenType, float[] returnValues) {
        if(DEBUG) Log.d(TAG, "getValues() tweenType: " + tweenType + ", returnValues: " + Arrays.toString(returnValues));

        Vector3 position = target.position;
        returnValues[POS_X] = position.x;
        returnValues[POS_Y] = position.y;
        returnValues[POS_Z] = position.z;

        Vector3 direction = target.direction;
        returnValues[DIRECTION_X] = direction.x;
        returnValues[DIRECTION_Y] = direction.y;
        returnValues[DIRECTION_Z] = direction.z;

        return VALUES_NUMBER;
    }

    @Override
    public void setValues(Camera target, int tweenType, float[] newValues) {
        if(DEBUG) Log.d(TAG, "setValues() tweenType: " + tweenType + ", newValues: " + Arrays.toString(newValues));

        Vector3 pos = target.position;
        pos.x = newValues[POS_X];
        pos.y = newValues[POS_Y];
        pos.z = newValues[POS_Z];

        Vector3 direct = target.direction;
        direct.x = newValues[DIRECTION_X];
        direct.y = newValues[DIRECTION_Y];
        direct.z = newValues[DIRECTION_Z];

        target.update();
    }

    public int getValuesNumber(){
        return VALUES_NUMBER;
    }

    public float[] setValues(float[] returnValues, Camera camera, Frame frame, float distance){
        Vector3 position = new Vector3(camera.position);
        Vector3 direction = new Vector3(camera.direction);
        // Get a new position
        frame.transformState(position, direction, distance);

        returnValues[POS_X] = position.x;
        returnValues[POS_Y] = position.y;
        returnValues[POS_Z] = position.z;

        returnValues[DIRECTION_X] = direction.x;
        returnValues[DIRECTION_Y] = direction.y;
        returnValues[DIRECTION_Z] = direction.z;

        return returnValues;
    }
}
