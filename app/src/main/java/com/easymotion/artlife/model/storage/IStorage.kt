package com.easymotion.artlife.model.storage

import android.net.Uri
import rx.Observable

/** The main app data storage. Storing images URIs of all sources */
interface IStorage {
    fun get(key: String): Observable<List<Uri>>
    fun addAll(key: String, uriList: Collection<Uri>): Observable<Unit>
    fun removeAll(key: String, uriList: Collection<Uri>): Observable<Unit>
}

