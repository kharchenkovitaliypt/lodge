package com.easymotion.artlife.model.imagecollection

import android.net.Uri
import rx.Observable

/**
 * The base interface for all image collections.
 * Collection may be as for internal images as from the internet
 */
interface IImageCollection {

    /** Title of this collection */
    val title: String

    /** Emit all URIs that representing this collection */
    val imageList: Observable<List<Uri>>

    /** Returns [Observable] that will be reflect the removing process */
    fun removeImageList(items: Collection<Uri>): Observable<Unit>

    /** Emit [State] changes */
    val state: Observable<State>

    enum class State {
        ADDED,
        REMOVED,
        NOTHING // Substitution of null
    }

    interface IRequest {
        /** Call when need add a new image to this collection */
        fun addImageList()
    }
}
