package com.easymotion.artlife.model;

import android.opengl.GLES20;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Disposable;
import com.easymotion.artlife.model.animation.TweenAnimation;
import com.easymotion.artlife.model.image.ITextureBox;
import com.easymotion.artlife.utils.Settings;

import org.jetbrains.annotations.Nullable;

public class Frame implements Disposable{
    private static final String TAG = Frame.class.getSimpleName();
    private static final long TEXTURE_ATTRIBUTE_TYPE = TextureAttribute.Diffuse;

    private static final float TRANSPARENT = 0;
    private static final int OPAQUE = 1;

    private Shape mCurrentShape;
    private Shape mTmpShape;

    private long mImageSetTime = 0;

    private final Vector3 mPosition = new Vector3();
    private final Vector3 mRotationAxis = new Vector3();
    private float mRotationAngle = 0;

    private IScheduler mScheduler;
    private Settings mSettings;
    private TweenAnimation mTweenAnimation;
    private boolean mAnimated = false;

//    private float mNormalizedWidth = -1;

    public Frame(IScheduler scheduler, Settings settings){
        this(scheduler, settings, null);
    }

    public Frame(IScheduler scheduler, Settings settings, Texture texture){
        this(scheduler, settings, texture, 0, 0, 0);
    }

    public Frame(IScheduler scheduler, Settings settings, Texture texture, float x, float y, float z){
        mScheduler = scheduler;
        mSettings = settings;
        mCurrentShape = new Shape(null, OPAQUE);
        mTmpShape = new Shape(null, TRANSPARENT);

        mPosition.set(x, y, z);
        setTexture(null, texture, false); // update transformation
    }

    public float getNormalizedWidth(){
        return mCurrentShape.getNormalizedWidth();
    }

    public Texture getTexture(){
        return mCurrentShape.getTexture();
    }

    public void setTweenAnimation(TweenAnimation tweenAnimation){
        mTweenAnimation = tweenAnimation;
    }

    public float getOpacity(){
        return mCurrentShape.getOpacity();
    }

    public void setOpacity(float opacity){
        mCurrentShape.setOpacity(opacity);
        mTmpShape.setOpacity(1f - opacity);
    }

    private void updateTransformation(){
        mCurrentShape.updateTransformation();
        mTmpShape.updateTransformation();
    }

//    public float getNormalizedWidth(){
//        return mNormalizedWidth;
//    }

//    public void setPosition(float x, float y, float z){
//        mPosition.set(x, y, z);
//        updateTransformation();
//    }
//
//    public void setRotation(float x, float y, float z, float angle){
//        mRotationAxis.set(x, y, z);
//        mRotationAngle = angle;
//        updateTransformation();
//    }
//    /**
//     * Focus the camera on a frame at a distance
//     *
//     * @param camera Customizable camera
//     * @param distance Distance between camera and frame
//     */
//    public void focusCamera(Camera camera, float distance){
//        transformState(camera.position, camera.direction, distance);
//        // reset slope
//        camera.up.set(0, 1, 0); // TODO Up vector calculation
//        camera.update();
//    }

    public void transformState(Vector3 position, Vector3 direction, float distance){
        direction.set(0, 0, 1)
                .rotate(mRotationAxis, mRotationAngle)
                .nor();

        position.set(direction)
                .scl(distance)
                .add(mPosition);

        // calculate the direction (invert frame direction)
        direction.set(-direction.x, -direction.y, -direction.z);
    }

    /**
     * Set a image in this frame with animation
     * @param texture The texture to use as the central part, or null to remove the central part
     * @return Previous image
     */
    @Nullable
    public void setTexture(final ITextureBox textureBox, final Texture texture, boolean animate){
        if(mAnimated) throw new IllegalStateException();

        mTmpShape.setTexture(texture);

        if(animate){
            mAnimated = true;
            final int duration = mSettings.getImageChangeDuration();

            mTweenAnimation.animateFrameTransparency(this, TRANSPARENT, duration, new TweenAnimation.OnCompleteListener() {
                @Override
                public void onComplete() {
                    switchShapes(textureBox);
                    mAnimated = false;
                }
            });
        } else {
            setOpacity(TRANSPARENT);
            switchShapes(textureBox);
        }
    }

    public boolean isAnimated(){
        return mAnimated;
    }

    private void switchShapes(ITextureBox textureBox){
        Texture prevTexture = mCurrentShape.setTexture(null);
        if(prevTexture != null) textureBox.releaseTexture(prevTexture);

        Shape tmp = mCurrentShape;
        mCurrentShape = mTmpShape;
        mTmpShape = tmp;

        mImageSetTime = System.currentTimeMillis();
        mScheduler.requestUpdate(mSettings.getImageChangeInterval());
    }

    /**
     * Get the elapsed time after the image set
     * @return Elapsed time in second
     */
    public long getImageLifetime(){
        return (System.currentTimeMillis() - mImageSetTime) / 1000;
    }

    public void render(ModelBatch modelBatch){
        mCurrentShape.render(modelBatch);
        mTmpShape.render(modelBatch);
    }

    /**
     * Disposes all resources held
     */
    @Override
    public void dispose() {
        mCurrentShape.dispose();
        mTmpShape.dispose();
    }


    class Shape implements Disposable{
        private final ModelInstance mModelInstance;

        private final Material mMaterial;
        private final TextureAttribute mTextureAttribute;
        private final BlendingAttribute mBlendingAttribute;

        private final Vector3 mScale = new Vector3(1, 1, 1);
        private float mNormalizedWidth;

        public Shape(Texture texture, float opacity){
            Model model = new ModelBuilder().createRect(
                    // Positions
                    -0.5f, -0.5f, 0,
                    0.5f, -0.5f, 0,
                    0.5f, 0.5f, 0,
                    -0.5f, 0.5f, 0,
                    // Normals
                    1, 1, 0,
                    GLES20.GL_TRIANGLES,
                    new Material(new BlendingAttribute(opacity),
                                 new TextureAttribute(TEXTURE_ATTRIBUTE_TYPE, texture)),
                    Usage.Position | Usage.TextureCoordinates);
            mModelInstance = new ModelInstance(model);

            mMaterial = mModelInstance.nodes.get(0).parts.get(0).material;
            mTextureAttribute =(TextureAttribute)mMaterial.get(TEXTURE_ATTRIBUTE_TYPE);
            mBlendingAttribute =(BlendingAttribute)mMaterial.get(BlendingAttribute.Type);
        }

        public float getNormalizedWidth(){
            return mNormalizedWidth;
        }

        public Texture getTexture(){
            return mTextureAttribute.textureDescription.texture;
        }

        public void render(ModelBatch modelBatch){
            if(getTexture() != null) {
                modelBatch.render(mModelInstance);
            }
        }

        void updateTransformation(){
            mModelInstance.transform
                    .idt()
                    .scl(mScale)
                    .rotate(mRotationAxis, mRotationAngle)
                    .trn(mPosition);
        }

        public float getOpacity(){
            return mBlendingAttribute.opacity;
        }

        public void setOpacity(float opacity){
            mBlendingAttribute.opacity = opacity;
        }

        private Texture setTexture(@Nullable Texture texture){
            // Configure the transformation to match the proportions of the image
            Texture prevTexture = mTextureAttribute.textureDescription.texture;
            mTextureAttribute.textureDescription.texture = texture;

            if(texture == null){
                mMaterial.remove(mTextureAttribute.type);
            } else {
                mNormalizedWidth = (float) texture.getWidth() / (float) texture.getHeight();
                mScale.set(mNormalizedWidth, 1f, 1f);
                updateTransformation();

                // Bind to the material texture
                mMaterial.set(mTextureAttribute);
            }
            return prevTexture;
        }

        @Override
        public void dispose(){
            mModelInstance.model.dispose();
        }
    }
}
