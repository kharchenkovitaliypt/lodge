package com.easymotion.artlife.model.image;

import android.net.Uri;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.Disposable;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public interface ITextureBox extends Disposable {

    void setImageURIs(Collection<Uri> imageURIs);

    @Nullable Texture obtainTexture();

    void releaseTexture(@NotNull Texture texture);

    int getTextureCount();

    boolean isEmpty();
}
