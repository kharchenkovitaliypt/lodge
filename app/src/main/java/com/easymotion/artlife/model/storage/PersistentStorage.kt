package com.easymotion.artlife.model.storage

import android.net.Uri
import com.easymotion.artlife.di.scopes.AppScope
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import rx.Observable
import rx.Scheduler
import rx.lang.kotlin.filterNotNull
import rx.lang.kotlin.toObservable
import rx.lang.kotlin.toSingletonObservable
import rx.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Provider

/** CRUD [Uri] list by key in [Realm] */
@AppScope
class PersistentStorage
@Inject constructor(val mRealmProvider: Provider<Realm>,
                    val mRealmScheduler: Scheduler)
: IStorage {

    /** Must be only called on [Realm]'s thread */
    fun getRealm(): Realm = mRealmProvider.get()

    override fun get(key: String): Observable<List<Uri>> {
        return getEntry(key)
                .flatMap {entry -> entry.getValues().toObservable() }
                .map {rUri -> Uri.parse(rUri.getValue()) }
                .toList()
    }

    override fun addAll(key: String, uriList: Collection<Uri>): Observable<Unit> {
        return transaction(key, uriList) {entry, rUriList ->
            entry.getValues().addAll(rUriList)
        }
    }

    override fun removeAll(key: String, uriList: Collection<Uri>): Observable<Unit> {
        return transaction(key, uriList) {entry, rUriList ->
            entry.getValues().removeAll(rUriList)
        }
    }

    /** Creates [Entry] if not exist, convert [Uri] list to [RUri] list
     * and within [Realm.executeTransaction] propagate them to the functional parameter(block) */
    private fun transaction(key: String,
                            uriList: Collection<Uri>,
                            block: (e: Entry, list: List<RUri>) -> Unit): Observable<Unit> {
        val entry = getEntry(key)
        .switchIfEmpty(createNewEntry(key))
        .cache()

        val rUriList = entry.map {entry -> findMatchRUriList(entry, uriList) }
        .filter { it.isNotEmpty() }
        .switchIfEmpty(createNewRUriList(uriList))

        val transaction = Observable.zip(entry, rUriList) { entry, rUriList ->
            getRealm().executeTransaction { block(entry, rUriList) }
        }
        .cache() // For a single execution
        .subscribeOn(mRealmScheduler)

        // Warm up. Start execution without a subscription
        // TODO Find a better solution
        transaction.observeOn(Schedulers.computation()).subscribe()
        return transaction
    }

    /** Looks for [RUri]s in [Entry] that match [Uri]s passed as parameters  */
    private fun findMatchRUriList(entry: Entry, uriList: Collection<Uri>): List<RUri> {
        val strUriList = uriList.map { it.toString() }
        return entry.getValues().filter {rUri -> strUriList.contains(rUri.getValue())}
    }

    /** Create [Observable] which will create new [Entry] and initiate its key from key parameter
     * and values with empty [RealmList] */
    private fun createNewEntry(key: String): Observable<Entry> {
        return Observable.fromCallable {
            var entry: Entry? = null
            getRealm().executeTransaction {
                entry = getRealm().createObject(Entry::class.java).apply {
                    setKey(key)
                    setValues(RealmList<RUri>())
                }
            }
            entry!! // Return no null Entry
        }
        .subscribeOn(mRealmScheduler)
    }

    /** Returns [Observable] that map [Uri] list into [Entry.RUri] list */
    private fun createNewRUriList(items: Collection<Uri>): Observable<List<RUri>> {
        return items.map {uri -> RUri(uri.toString()) }.toSingletonObservable()
    }

    /**
     * Look for and produce [Entry] that match the method param 'key'
     * and perform that on the Realm's scheduler
     */
    private fun getEntry(key: String): Observable<Entry> {
        return Observable.fromCallable {
            getRealm().where(Entry::class.java).equalTo("key", key).findFirst()
        }
        .filterNotNull()
        .subscribeOn(mRealmScheduler)
    }
}

/** Key-values pair that compatible with [Realm] */
open class Entry : RealmObject() {
    @PrimaryKey
    private lateinit var key: String
    private lateinit var values: RealmList<RUri>

    open fun getKey(): String = key
    open fun setKey(key: String) { this.key = key }

    open fun getValues(): RealmList<RUri> = values
    open fun setValues(values: RealmList<RUri>) { this.values = values }
}

/** A mediator from internal Realm native [RealmObject] to external [Uri]  */
open class RUri : RealmObject {
    private lateinit var value: String
    constructor()
    constructor(value: String) { this.value = value }

    open fun getValue(): String = value
    open fun setValue(value: String) { this.value = value }
}