package com.easymotion.artlife.model.animation;

import com.easymotion.artlife.model.Frame;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.Nullable;

import javax.inject.Inject;

import aurelienribon.tweenengine.BaseTween;
import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenCallback;
import aurelienribon.tweenengine.TweenManager;

public class TweenAnimation {
    private final TweenManager mTweenManager = new TweenManager();
    private final EventBus mBus = new EventBus();
    private boolean mIsAnimated = false;

    public enum AnimationStatus{ START, COMPLETE }

    @Inject
    public TweenAnimation() {}

    public boolean isAnimated(){
        return mIsAnimated;
    }

    public void setAnimated(boolean animated){
        mIsAnimated = animated;
        mBus.post(animated ? AnimationStatus.START : AnimationStatus.COMPLETE);
    }

    public void animateFrameTransparency(final Frame frame, float to, float duration, final OnCompleteListener listener) {
        FrameAccessor tweenAccessor = (FrameAccessor) getTweenAccessor(Frame.class, FrameAccessor.class);
        if (tweenAccessor == null) return;

        Tween.to(frame, FrameAccessor.TYPE_TRANSPARANCY, duration)
                .target(to)
                .setCallback(new TweenCallback() {
                    @Override
                    public void onEvent(int event, BaseTween<?> baseTween) {
                        if (TweenCallback.COMPLETE == event) {
                            setAnimated(false);
                            listener.onComplete();
                        }
                    }
                })
                .start(mTweenManager);

        setAnimated(true);
    }

    public void register(Object obj){
        mBus.register(obj);
    }

    public void unregister(Object obj){
        mBus.unregister(obj);
    }

//    public void animateCamera(final Camera camera, final Frame frame, TweenCallback tweenCallback) {
//        CameraAccessor tweenAccessor = (CameraAccessor) getTweenAccessor(Camera.class, CameraAccessor.class);
//        if (tweenAccessor == null) return;
//
//        float[] targetValues = new float[tweenAccessor.getValuesNumber()];
//        tweenAccessor.setValues(targetValues, camera, frame, mSettings.getCameraDistance());
//
//        Tween.to(camera, -1, mSettings.getCameraMovementDuration())
//                .delay(mSettings.getCameraMovementInterval())
//                .ease(Expo.INOUT)
//                .target(targetValues)
//                .setCallback(tweenCallback)
//                .start(mTweenManager);
//    }

    @Nullable
    private TweenAccessorExt<?> getTweenAccessor(Class targetClass, Class accessorClass) {
        TweenAccessorExt<?> tweenAccessor =
                (TweenAccessorExt<?>) Tween.getRegisteredAccessor(targetClass);
        if (tweenAccessor == null) {
            try {
                tweenAccessor = (TweenAccessorExt<?>) accessorClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
            Tween.registerAccessor(targetClass, tweenAccessor);
        }
        Tween.setCombinedAttributesLimit(CameraAccessor.VALUES_NUMBER); // TODO Set the greatest value of all accessor

        return tweenAccessor;
    }

    public void update(float deltaTime) {
        mTweenManager.update(deltaTime);
    }

    public interface OnCompleteListener {

        void onComplete();
    }
}
