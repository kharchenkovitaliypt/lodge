package com.easymotion.artlife.model.image;

import android.net.Uri;
import android.util.Log;

import com.badlogic.gdx.graphics.Texture;
import com.easymotion.artlife.BuildConfig;
import com.easymotion.artlife.utils.ConfigResolver;
import com.easymotion.artlife.utils.Utils;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class EasyTextureBox implements ITextureBox {
    private static final int INIT_SIZE = 32;
    private final List<Uri> mImageURIs = new ArrayList<>(INIT_SIZE);
    private final ConfigResolver mConfigResolver;

    public EasyTextureBox(Collection<Uri> items, ConfigResolver resolver){
        mConfigResolver = resolver;
        setImageURIs(items);
    }

    @Override
    public void setImageURIs(Collection<Uri> allImageURIs) {
        mImageURIs.retainAll(allImageURIs);

        Collection<Uri> newItems = Utils.INSTANCE.removeAll(allImageURIs, mImageURIs);
        mImageURIs.addAll(newItems);
    }

    @Nullable
    @Override
    public Texture obtainTexture() {
        if(BuildConfig.DEBUG) Log.d("EasyTextureBox", "obtainTexture(), mImageUris count: " + mImageURIs.size());
        if(mImageURIs.isEmpty()) return null;

        Collections.rotate(mImageURIs, 1);

        Texture texture = null;
        while(texture == null && !mImageURIs.isEmpty()){
            Uri uri = mImageURIs.get(0);
            texture = TextureFactory.createTexture(uri, mConfigResolver);
            if(texture == null){
                Log.e("EasyTextureBox", "Damaged image or unsupported format: " + uri);
                mImageURIs.remove(uri);
            }
        }
        return texture;
    }

    @Override
    public void releaseTexture(@NotNull Texture texture) {
        texture.dispose();
    }

    @Override
    public int getTextureCount(){
        return mImageURIs.size();
    }

    @Override
    public boolean isEmpty() {
        return mImageURIs.isEmpty();
    }

    @Override
    public void dispose() {}
}
