package com.easymotion.artlife.model.animation;

import aurelienribon.tweenengine.TweenAccessor;

public interface TweenAccessorExt<T> extends TweenAccessor<T> {

    int getValuesNumber();
}
