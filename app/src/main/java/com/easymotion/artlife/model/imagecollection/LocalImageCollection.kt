package com.easymotion.artlife.model.imagecollection

import android.app.Activity
import android.content.Intent
import android.net.Uri
import com.easymotion.artlife.R
import com.easymotion.artlife.di.scopes.ActivityScope
import com.easymotion.artlife.di.scopes.AppScope
import com.easymotion.artlife.interfaces.IResources
import com.easymotion.artlife.model.storage.IStorage
import com.easymotion.artlife.system.ActivityResultHandler
import com.easymotion.artlife.utils.ActivityRequestCode
import com.easymotion.artlife.utils.Config
import com.luminous.pick.Action
import com.luminous.pick.Extras
import javax.inject.Inject

@AppScope
class LocalImageCollection
@Inject
constructor(uriStorage: IStorage, res: IResources)
: AbsImageCollection(uriStorage, res.getString(R.string.storage_local)) {

    override val mStorageKey: String = "local"

    override val defaultUriList: List<Uri> = emptyList()

    /** Separate the [ActivityScope] and [AppScope] logic */
    @ActivityScope
    class Request
    @Inject constructor(config: Config,
                        private val mLocalImageCollection: LocalImageCollection,
                        private val mActivityResultHandler: ActivityResultHandler)
    : IImageCollection.IRequest, ActivityResultHandler.OnActivityResultListener {

        init { mActivityResultHandler.register(this) }

        private val REQUEST_CODE = ActivityRequestCode.generate()
        private val SCHEME_FILE = "file://"
        private val SUPPORTED_EXTENSIONS = config.getStringList(R.string.supported_image_extensions)

        override fun addImageList() {
            // Show files chooser
            val intent = Intent(Action.ACTION_MULTIPLE_PICK)
            mActivityResultHandler.startActivity(intent, REQUEST_CODE)
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (REQUEST_CODE != requestCode) return
            if (Activity.RESULT_OK != resultCode || data == null) return

            val allPaths = data.getStringArrayExtra(Extras.ALL_PATHS) ?: return
            val filteredFiles = allPaths.map { path -> "$SCHEME_FILE$path" } // Append file scheme to file path
                                        .map { Uri.parse(it) }
                                        .filter { hasSupportedExtension(it) }
            mLocalImageCollection.addImageList(filteredFiles)
        }

        /**
         * Check URI file extension is in the [Request.SUPPORTED_EXTENSIONS] list
         */
        fun hasSupportedExtension(uri: Uri): Boolean {
            val pos = uri.path.lastIndexOf('.')
            if(pos == -1) return false // path has no extension

            val extension = uri.path.substring(pos + 1)
            return SUPPORTED_EXTENSIONS.contains(extension)
        }
    }
}
