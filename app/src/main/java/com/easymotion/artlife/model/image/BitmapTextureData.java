package com.easymotion.artlife.model.image;

import android.graphics.Bitmap;
import android.net.Uri;
import android.opengl.GLUtils;
import android.util.Log;

import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.TextureData;
import com.badlogic.gdx.utils.GdxRuntimeException;
import com.easymotion.artlife.BuildConfig;
import com.easymotion.artlife.utils.ConfigResolver;

import java.io.IOException;

public class BitmapTextureData implements TextureData {
    private static final boolean DEBUG = false && BuildConfig.DEBUG;
    private final Uri mUri;
    private final ConfigResolver mConfigResolver;

    private Bitmap mBitmap;
    private Pixmap.Format mFormat;
    private int mWidth;
    private int mHeight;

    private boolean mIsPrepared = false;
    private boolean mIsInitialized = false;

    public BitmapTextureData(Uri uri, ConfigResolver resolver){
        mUri = uri;
        mConfigResolver = resolver;
    }

    @Override
    public TextureDataType getType () {
        return TextureDataType.Custom;
    }

    @Override
    public boolean isPrepared () {
        return mIsPrepared;
    }

    public boolean isInitialized(){
        return mIsInitialized;
    }

    @Override
    public void prepare () {
        if(DEBUG) Log.d("BitmapTextureData", "prepare() " + mUri);
        try {
            mBitmap = mConfigResolver.resolveBitmap(mUri);

            Bitmap.Config config = mBitmap.getConfig();
            if(config == null){
                Log.e("BitmapTextureData", "Unknown bitmap config: " + mUri);
                return;
            }
            mFormat = fromBitmapConfig2PixmapFormat(config);
            mWidth = mBitmap.getWidth();
            mHeight = mBitmap.getHeight();

            mIsInitialized = true;
            mIsPrepared = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void consumeCustomData (int target) {
        if(DEBUG) Log.d("BitmapTextureData", "consumeCustomData() " + mUri);
        if(mBitmap != null) {
            GLUtils.texImage2D(target, 0, mBitmap, 0);
            mBitmap.recycle();
            mBitmap = null;

            mIsPrepared = false;
        }
    }

    @Override
    public Pixmap consumePixmap () {
        throw new GdxRuntimeException("It's compressed, use the compressed method");
    }

    @Override
    public boolean disposePixmap () {
        return false;
    }

    @Override
    public int getWidth () {
        return mWidth;
    }

    @Override
    public int getHeight () {
        return mHeight;
    }

    @Override
    public Pixmap.Format getFormat () {
        return mFormat;
    }

    private Pixmap.Format fromBitmapConfig2PixmapFormat(Bitmap.Config config){
        switch (config){
            case ARGB_4444:
                return Pixmap.Format.RGBA4444;
            case ARGB_8888:
                return Pixmap.Format.RGBA8888;
            case RGB_565:
                return Pixmap.Format.RGB565;
            default:
                return null;
        }
    }

    @Override
    public boolean useMipMaps () {
        return false;
    }

    @Override
    public boolean isManaged () {
        return true;
    }
}
