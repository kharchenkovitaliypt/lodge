package com.easymotion.artlife.model.imagecollection

import android.net.Uri
import com.easymotion.artlife.model.imagecollection.IImageCollection.State
import com.easymotion.artlife.model.storage.IStorage
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.subjects.PublishSubject

/**
 * Abstract source
 */
abstract class AbsImageCollection(private var mStorage: IStorage,
                                  override val title: String)
: IImageCollection {
    /**  Key with which items will be saved and restored */
    protected abstract val mStorageKey: String

    /** Default [Uri] list */
    protected abstract val defaultUriList: List<Uri>

    override val imageList: Observable<List<Uri>>
        get() = mStorage.get(mStorageKey)
                .switchIfEmpty(Observable.fromCallable { defaultUriList })

    override val state: PublishSubject<State> = PublishSubject.create<State>()

    override fun removeImageList(items: Collection<Uri>): Observable<Unit> {
        val observable = mStorage.removeAll(mStorageKey, items)
        // Subscribes to onComplete to notify about state changes
        observable.observeOn(AndroidSchedulers.mainThread())
                  .subscribe({}, {}, { state.onNext(State.REMOVED) }) // On complete
        return observable
    }

    protected fun addImageList(items: Collection<Uri>): Observable<Unit> {
        val observable = mStorage.addAll(mStorageKey, items)
        // Subscribes to onComplete to notify about state changes
        observable.observeOn(AndroidSchedulers.mainThread())
                  .subscribe({}, {}, { state.onNext(State.ADDED) }) // On complete
        return observable
    }
}
