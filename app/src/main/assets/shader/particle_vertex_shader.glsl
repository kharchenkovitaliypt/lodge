uniform mat4 uMVPMatrix;

// Attributes
attribute vec3 aPoint;

void main(){

    gl_Position = uMVPMatrix * vec4(aPoint, 1.0);
    gl_PointSize = 5.0;
}