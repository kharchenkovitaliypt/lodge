#ifdef GL_ES
precision mediump float;
#endif

#define PI 3.14159
#define K  0.1
#define AURA 0.4

uniform vec2 u_resolution;

const vec2 center = vec2(0.5, 0.5) ;

void main( void ) {
    vec2 p;
	p.x = gl_FragCoord.x / u_resolution.x;
	p.y = gl_FragCoord.y / u_resolution.y;

	float dist = length(p - center);
	float a = K / dist - K / center[0] - (1.0 - AURA);

	gl_FragColor = vec4 (1.0, 1.0, 1.0, a);
}
