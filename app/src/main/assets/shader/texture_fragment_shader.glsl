precision mediump float;

uniform sampler2D uTexture;    // The input texture.
varying vec2 vTexCoordinate; // Interpolated texture coordinate per fragment.

void main(){

    gl_FragColor = texture2D(uTexture, vTexCoordinate);
}