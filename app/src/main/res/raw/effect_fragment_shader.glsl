precision highp float;

uniform float uTime;
vec2 uResolution = vec2(800, 800);

varying vec4 vColor;

const float PI = 3.1415926535;
const float t3 = PI * 2. / 3.;
const float t5 = PI * 4. / 3.;
const float POINT = 20.;

void main( void ) {

    vec2 p=(gl_FragCoord.xy -.5 * vec2(uResolution))/ min(uResolution.x,uResolution.y);

    vec3 c = vec3(0);

	float t1 = (1.5 + .3 * POINT) * PI;
	float t2 = uTime*0.05;
	float t4 = uTime*0.03;
	float t6 = (0.005+0.003*POINT);

    for(int i = 0; i < 12; i++){

		float t = t1 * float(i+3) / 10. * t2;

		float x = -.02 * cos(2.*t) * sin(2.*t);
		float y = .02 * sin(2.*t);
		float z = .02 * cos(2.*t);
		vec2 o = 1.45 * vec2(x,y);

		float hue = t4*float(i+5);

		float r = max(sin(hue)+0.15, 0.1);
		float g = max(sin(hue+t3)+0.15, 0.1);
		float b = max(sin(hue+t5)+0.15, 0.1);
     	c += .003/(length(p-o*5.*(1.2-cos(uTime*(t6*float(i+7))))))*vec3(r,g,b);
    }
    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0); // vec4(c * 1.2, 1);
}

