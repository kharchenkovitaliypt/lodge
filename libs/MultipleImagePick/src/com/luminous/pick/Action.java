package com.luminous.pick;

public interface Action {
	String ACTION_PICK = "luminous.ACTION_PICK";
	String ACTION_MULTIPLE_PICK = "luminous.ACTION_MULTIPLE_PICK";
}
